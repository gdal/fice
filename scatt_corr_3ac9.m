function  [ap_corr, bp_corr] = scatt_corr_3ac9(cp, ap, wv);

    refNIR = find(wv>=714,1);  %reference wavelength in the NIR

	bp = cp-ap;  %compute first estimate of scattering coefficient
	
	ap_corr = ap - ap(:,refNIR)./bp(:,refNIR) .* bp;  %apply eq.12b of the ac9 manual (ac9p.pdf)

    bp_corr = cp - ap_corr;
	
endfunction	
	
	
	
