# process AC9 data
clear all
close all

graphics_toolkit('qt');

PLOT = true;

% din = "../Data/FRM4SOC_ac9_data/";
din = "../Data/ac9/";
dout = "./output/ac9/";

fnday = glob([din "2018071[3-7]"]);









for iday = 1:5#length(fnday)

	dday = fnday{iday}(end-7:end);

	out.wv = [412   440   488   510   532   554   650   676   715];

	fn = glob([fnday{iday} "/*dat"]);
	fn2 = dir([fnday{iday} "/*dat"]);
	
	for ifn = 1:length(fn)
		fld = strrep(fnday{iday}(end-11:end), "/", "_");
		out.(fld).(fn2(ifn).name(1:end-4)) = rd_ac9_wetview(fn{ifn});
	endfor
	
	
	
	# find different types of measurements
		flds = fieldnames(out.(["ac9_" dday]));	
	
		MQs = find(cellfun(@isempty, strfind(flds, "MQ"))==0);
		CDOMs = find(cellfun(@isempty, strfind(flds, "CDOM"))==0);
		TOTs = find(cellfun(@isempty, strfind(flds, "TOT"))==0);
	

	# define which triplets of data to use (done manually)
		iMQ2run{1} = MQs(2:end);
		iCDOM2run{1} = CDOMs(end-1);
		iTOT2run{1} = TOTs(2:end-1);
		iaPART{1,2} = [1]; # CDOM
		iaPART{1,1} = [1]; # corresponding aTOT spectra
		icPART{1,2} = [1]; # CDOM
		icPART{1,1} = [1]; # corresponding cTOT spectra
		
		iMQ2run{2} = MQs([1 3:end]);
		iCDOM2run{2} = CDOMs(1:3);
		iTOT2run{2} = TOTs([1 3]);
		iaPART{2,2} = [1 3]; # CDOM
		iaPART{2,1} = [1 2]; # corresponding aTOT spectra
		icPART{2,2} = [1 3]; # CDOM
		icPART{2,1} = [1 2]; # corresponding cTOT spectra
		
		iMQ2run{3} = MQs(:);
		iCDOM2run{3} = CDOMs(1:3);
		iTOT2run{3} = TOTs(:);
		iaPART{3,2} = [1:3]; # CDOM
		iaPART{3,1} = [1:3]; # corresponding aTOT spectra
		icPART{3,2} = [1:3]; # CDOM
		icPART{3,1} = [1:3]; # corresponding cTOT spectra
		
		iMQ2run{4} = MQs(1:end-1);
		iCDOM2run{4} = CDOMs(:);
		iTOT2run{4} = TOTs(:);
		iaPART{4,2} = [1:4]; # CDOM
		iaPART{4,1} = [1:4]; # corresponding aTOT spectra
		icPART{4,2} = [1:4]; # CDOM
		icPART{4,1} = [1:4]; # corresponding cTOT spectra
		
		iMQ2run{5} = MQs(1:3);
		iCDOM2run{5} = CDOMs(1);
		iTOT2run{5} = TOTs(1:2);
		iaPART{5,2} = [1]; # CDOM
		iaPART{5,1} = [1]; # corresponding aTOT spectra
		icPART{5,2} = [1]; # CDOM
		icPART{5,1} = [1]; # corresponding cTOT spectra
		


	# compute median MQ
		tmp_a = tmp_c = [];
		for irec = 1:length(iMQ2run{iday})
			tmp_a = [tmp_a; out.(fld).(flds{iMQ2run{iday}(irec)}).a];
			tmp_c = [tmp_c; out.(fld).(flds{iMQ2run{iday}(irec)}).c];
		endfor
	
		out.(fld).MQ.a = median(tmp_a);
		out.(fld).MQ.a_err = prcrng(tmp_a);
		out.(fld).MQ.a16prct = prctile(tmp_a,16);
		out.(fld).MQ.a84prct = prctile(tmp_a,84);
		out.(fld).MQ.c = median(tmp_c);
		out.(fld).MQ.c_err = prcrng(tmp_c);
		out.(fld).MQ.c16prct = prctile(tmp_c,16);
		out.(fld).MQ.c84prct = prctile(tmp_c,84);
		
			
		# compute aTOT and cTOT
			for irec = 1:length([iTOT2run{iday}])
				out.(fld).TOT.a(irec,:) = median(out.(fld).(flds{iTOT2run{iday}(irec)}).a) - out.(fld).MQ.a;	
				out.(fld).TOT.a_err(irec,:) = sqrt(prcrng(out.(fld).(flds{iTOT2run{iday}(irec)}).a).^2 + out.(fld).MQ.a_err);	
				out.(fld).TOT.c(irec,:) = median(out.(fld).(flds{iTOT2run{iday}(irec)}).c) - out.(fld).MQ.c;	
				out.(fld).TOT.c_err(irec,:) = sqrt(prcrng(out.(fld).(flds{iTOT2run{iday}(irec)}).c).^2 + out.(fld).MQ.c_err);	

				# apply scattering correction to aTOT
				out.(fld).TOT.a(irec,:) = scatt_corr_3ac9(out.(fld).TOT.c(irec,:),out.(fld).TOT.a(irec,:), out.wv);	
			endfor
			
		# compute aCDOM and cCDOM
			for irec = 1:length([iCDOM2run{iday}])
				out.(fld).CDOM.a(irec,:) = median(out.(fld).(flds{iCDOM2run{iday}(irec)}).a) - out.(fld).MQ.a;	
				out.(fld).CDOM.a(irec,:) = out.(fld).CDOM.a(irec,:) - out.(fld).CDOM.a(irec,out.wv==650);	
				out.(fld).CDOM.a(irec,out.wv>650) = 0;
				out.(fld).CDOM.a_err(irec,:) = sqrt(prcrng(out.(fld).(flds{iCDOM2run{iday}(irec)}).a).^2 + out.(fld).MQ.a_err);	
				out.(fld).CDOM.c(irec,:) = median(out.(fld).(flds{iCDOM2run{iday}(irec)}).c) - out.(fld).MQ.c;	
				out.(fld).CDOM.c(irec,:) = out.(fld).CDOM.c(irec,:) - out.(fld).CDOM.c(irec,out.wv==650);
				out.(fld).CDOM.c(irec,out.wv>650) = 0;	
				out.(fld).CDOM.c_err(irec,:) = sqrt(prcrng(out.(fld).(flds{iCDOM2run{iday}(irec)}).c).^2 + out.(fld).MQ.c_err);	
			endfor
		
		% compute daily median values
		if size(out.(fld).CDOM.a, 1)>1
			out.(fld).medians.aCDOM = median(out.(fld).CDOM.a);
		else
			out.(fld).medians.aCDOM = (out.(fld).CDOM.a);
		endif
		if size(out.(fld).CDOM.c, 1)>1
			out.(fld).medians.cCDOM = median(out.(fld).CDOM.c);
		else
			out.(fld).medians.cCDOM = (out.(fld).CDOM.c);
		endif
		
		if size(out.(fld).TOT.a, 1)>1
			out.(fld).medians.aTOT = median(out.(fld).TOT.a);
		else
			out.(fld).medians.aTOT = (out.(fld).TOT.a);
		endif
		if size(out.(fld).TOT.c, 1)>1
			out.(fld).medians.cTOT = median(out.(fld).TOT.c);
		else
			out.(fld).medians.cTOT = (out.(fld).TOT.c);
		endif
		
		
			
		% compute ap and cp
			if length([iaPART{iday,1}])>1
				for irec = 1:length([iaPART{iday,1}])
					out.(fld).medians.ap(irec,:) = median(out.(fld).(flds{iTOT2run{iday}(iaPART{iday,1}(irec))}).a) - median(out.(fld).(flds{iCDOM2run{iday}(iaPART{iday,2}(irec))}).a);
					out.(fld).medians.ap(irec,:) = out.(fld).medians.ap(irec,:) - out.(fld).medians.ap(irec,out.wv==715);
				endfor
			else
				out.(fld).medians.ap = out.(fld).medians.aTOT - out.(fld).medians.aCDOM;
			endif
		
			if length([icPART{iday,1}])>1
				for irec = 1:length([icPART{iday,1}])
					out.(fld).medians.cp(irec,:) = median(out.(fld).(flds{iTOT2run{iday}(icPART{iday,1}(irec))}).c) - median(out.(fld).(flds{iCDOM2run{iday}(icPART{iday,2}(irec))}).c);
				endfor
			else
				out.(fld).medians.cp = out.(fld).medians.cTOT - out.(fld).medians.cCDOM;
			endif
	
				
		if PLOT
		# plot time series for each variable for each day
			FICE_plot_ac9(out, iMQ2run{iday}, "MQ", dout, fld, flds, dday);
			FICE_plot_ac9(out, iCDOM2run{iday}, "CDOM", dout, fld, flds, dday);
			FICE_plot_ac9(out, iTOT2run{iday}, "TOT", dout, fld, flds, dday);
		endif
	
	

endfor	


	
	if iday==1
		
		
		
		
		
		
	endif	
	