# this script is used to compute the uncertainty budget for the data collected at Tartu Observatory 
# during the FRM4SOC experiment conducted in May 2017
function proc_TO_experiment(input_parameters, main_dir)

    used_pixels = 15:194; # these are the only wavelengths for which Tartu Obs supplied a calibration

    addpath = '~/Dropbox/Octave_functions/hsas/';

    # read cals
    dir_cal = [main_dir 'TO_HyerpSAS_Cal_11Nov2017/'];


    ###### ENTER HERE THE INPUT PARAMATERS ###########
        din = input_parameters.din;
        ser_no = input_parameters.ser_no;
        fn = input_parameters.fn;
    ##################################################


    fncal = glob([dir_cal '*' ser_no '.cal']);
    cal = hsas_rd_satlantic_cal(fncal{1}, used_pixels);


    # read all data (in raw counts)
    
    # process each file 
    for ifn = 1:length(fn)

        # create field name
        fld = ["_" strrep(strsplit(fn{ifn}, {'/','.'}){end-1}, "-", "_")];

        # what instrument?
        sn = strsplit(fld, "_"){3};
        switch sn
            case "258"
                instr = "ES";
            case "222"
                instr = "LI";
            case "223"
                instr = "LT";
        endswitch
        
        # read files
        out.(fld) = hsas_rd_digital_counts(fn{ifn}, used_pixels);


        # calibrate files using TO calibration
            ff = [instr "cal"];
            out.(fld).(ff) = hsas_calibrate(L_CountsLightDat=out.(fld).(instr), L_CalDarkDat=cal.offset, cal.gain, immers_coeff=1, it_1=cal.int_time_wv, it_2=out.(fld).int_time_sec, used_pixels);
                # fn = glob ([din '*1024*LHL-HSE258.*']);
                % out.EScaldk = apply_cal(outdk.ES, cal.offset, cal.gain, immers_coeff=1, cal.int_time, outdk.int_time_sec);
                # # read hsas file calibrated using satcon to check my calibration function
                # fnproc = glob (['~/Dropbox/Project/FRM4SOC/TO/Tartu/20170510_Tartu/processed_TO_cal_11Nov17/20170510_258_1024_100_LHL-HSE258.dat']);
                # 
                # sn{2}='258';
                # outproc = hsas_rd(fnproc{1});
                
        # sort fields alphabetically
            out.(fld) = orderfields(out.(fld));
        
        
        ####### compute uncertainty: radiometric calibration
#             out.(fld).unc.rad_cal = cmp_unc_rad_cal(out.(fld).(ff), sn, L_CountsLightDat, L_CalDarkDat, cal.gain, immers_coeff, it_1, it_2);

     


    endfor            



    wv = out.(fld).wv;
                
    # compute average dark

        # extract label from file name (to figure out which files are the dark counts)
            flds = fieldnames (out);
            
            dk.all = [];
            
            for ifn = 1:length(flds)
                if ~isempty(strfind(flds{ifn}, "D")) # find files with dark counts
                    dk.(flds{ifn}) = out.(flds{ifn}).(ff);
                    dk.all = [dk.all; dk.(flds{ifn})];
                    isdark(ifn) = true;
                else
                    isdark(ifn) = false;
                endif
            endfor

            dk.([ff "_nDARK"]) = size(dk.all,1);
            dk.([ff "_avgDARK"]) = mean(dk.all);
            dk.([ff "_steDARK"]) = std(dk.all)/sqrt(dk.([ff "_nDARK"]));
            
            
    # compute average uncorrected (for dark) (ir)radiance
            flds = fieldnames (out);
            
            unc.all = [];
            
            for ifn = 1:length(flds)
                if isempty(strfind(flds{ifn}, "D"))
                    unc.(flds{ifn}) = out.(flds{ifn}).(ff);
                    unc.all = [unc.all; unc.(flds{ifn})];
                    isnotdark(ifn) = true;
                else
                    isnotdark(ifn) = false;
                endif
            endfor


            unc.([ff "_n"]) = size(unc.all,1);
            unc.([ff "_avg"]) = mean(unc.all);
            unc.([ff "_ste"]) = std(unc.all)/sqrt(unc.([ff "_n"]));




    # compute intensities: remove avg dark from avg irradiance
        OUT.avg = unc.([ff "_avg"]) - dk.([ff "_avgDARK"]); # [uW/cm2/nm{/sr}]
        OUT.ste = sqrt(unc.([ff "_ste"]).^2 + dk.([ff "_steDARK"]).^2); # [uW/cm2/nm{/sr}]
        
    # compute uncertainty due to radiometric calibration       
        OUT.u_cal_prc = cmp_unc_rad_cal(OUT.avg, sn)./OUT.avg*100;;# [%]
        
    # compute uncertainty due to alignment and [polarisation_ES | back-reflection_Ls ]
        OUT.u_pol_align = cmp_unc_pol_align(out, sn);;# [%]
        
    # compute uncertainty in the linearity correction
        OUT.u_lin_corr = cmp_unc_lin_correction(out, sn);;# [%]
       
        
        
        
        
        
        
        
    # convert units
        convert_units = inline("in/1e3*1e4", "in"); # from [uW/cm2/nm{/sr}] to [mW/cm2/nm{/sr}] to [mW/ m2/nm{/sr}]
        
        ff = fieldnames(OUT);
        for iff=1:length(ff)
            if ff{iff}(1)=="u", continue; endif
            OUT.(ff{iff}) = convert_units(OUT.(ff{iff}));  # [mW/ m2/nm{/sr}]
        endfor


    
        
        
        
        
    # simulate OLCI bands
        for iff=1:length(ff)
            [OUT.olci.(ff{iff}), OUT.olci.wv, OUT.olci.wv_int] = cmpOLCIbands(OUT.(ff{iff}), wv); # [mW/ m2/nm{/sr}]
        endfor
    
        
        
        
        
        
        
        
        
        
        
        
        
    # report results
    format short g
    disp( "\n------------------------------------------------------------------------------------------------") 
    disp([ input_parameters.sheet]);
    disp(["\t\t\t"              sprintf([repmat("%3.1f\t\t", 1,7) ],        OUT.olci.wv)               ]);
    disp(["\tintensity\t"       sprintf([repmat("%3.3f\t\t", 1,7) ],        OUT.olci.avg)              ]);
    disp("--------------------") 
    disp(["\tu_cal(%)\t"    sprintf([repmat("%3.3f\t\t", 1,7) ],    OUT.olci.u_cal_prc)    ]);
    disp(["\tu_pol_align(%)\t"    sprintf([repmat("%3.3f\t\t", 1,7) "\n"],    OUT.olci.u_pol_align)    ]);
    
    
    
    
    
    # write results in xls file
    fnout = '/fast_scratch/TO/tmp.xlsx';
    OUT.olci.avg'
    status = xlswrite(fnout, OUT.olci.avg', input_parameters.sheet, 'B5:H5');

    
endfunction








