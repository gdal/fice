function FICE_plot_ac9(d, ia, a_name, dout, fld, flds, dday)
# d: out.ac9_20180713.AAOT_20180713_0855_MQ.a and .c
	
# plot MQ of first day
	cmap = colormap('jet');
	cmap = cmap(1:7:60,:); # select only 9 colors
	
	# extract times 4 plotting:
		for iF = 1:length(flds)
			tmp = strsplit(flds{iF}, "_");
			tm(iF) = datenum([tmp{2} "_" tmp{3}], "yyyymmdd_HHMM");
		endfor
		tm0 = tm(1);
		tm = tm-tm(1);
		

	clf
	figure(1, 'visible', 'off')
	for iF = 1:length(ia)
	 subplot(121)
		hold on
		for iwv = 1:9
			plot(tm(ia(iF))+d.(fld).(flds{ia(iF)}).time_ms/1000/60/1440, d.(fld).(flds{ia(iF)}).a(:,iwv), '-', lw, 1, 'color', cmap(iwv,:))
		endfor
		grid on
		% ylim([-0.35 -0.05])
		ylabel('raw a [m^{-1}]')
		title([a_name ' measurements,  ' dday])
		set(gca, fs, 20)
		xlim([0 0.22])
		ylim([-0.35 0.30])
						
	 subplot(122)
		hold on
		for iwv = 1:9
			plot(tm(ia(iF))+d.(fld).(flds{ia(iF)}).time_ms/1000/60/1440, d.(fld).(flds{ia(iF)}).c(:,iwv), '-', lw, 1, 'color', cmap(iwv,:))
		endfor
		set(gca, fs, 20)
		grid on
		% ylim([-0.35 -0.05])
		ylabel('raw c [m^{-1}]')
		xlim([0 0.22])
		ylim([0.4 3.0])
				
	endfor
	
	set(gcf, 'paperposition', [0.25 0.25 14 6])
	
	fnout = [dout dday "_" a_name ".png"];
	print("-dpng", fnout)		
	
	
endfunction