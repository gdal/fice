# compute uncertainy budget for FICE rrs data based on Martin's xls data
clear all


#		wv		u2_Rrs		u2_Lsky		u2_Lu		u2_Ed		u2_rho		dRrs2dLsky	dRrs2dLu	dRrs2dEd	dRrs2drho		1_Lu2Ed		r_Lsky_Lu	r_Lsky_Ed	r_Lu_Ed
d = [	400		2.42E-07	513.5314271	0.617357158	8375.048724	9.24E-06	-3.07E-05	0.001103745	-5.16E-06	-0.095972497	0.007324306	0.74229952	0.383497651	0.745286982
		412.5	1.91E-07	742.8027268	0.769541536	8989.935039	9.06E-06	-2.49E-05	0.000894906	-4.24E-06	-0.089226712	0.007198567	0.781272286	0.365530088	0.699296615
		442.5	1.28E-07	889.5666009	0.99782164	10956.88698	9.64E-06	-2.20E-05	0.000785479	-3.95E-06	-0.073529434	0.007067982	0.815904972	0.37186142	0.735957991
		490		1.12E-07	937.3289203	1.150074616	12588.53118	1.17E-05	-2.04E-05	0.000718387	-4.36E-06	-0.0579115		0.007682577	0.817614557	0.352516312	0.745820707
		510		8.91E-08	855.015715	1.035252497	12093.80519	1.30E-05	-2.09E-05	0.000729028	-4.33E-06	-0.052784249	0.007417664	0.807231029	0.354071243	0.770706621
		560		1.15E-07	685.7906469	0.764999678	12150.56587	1.70E-05	-2.17E-05	0.000749416	-4.00E-06	-0.043052671	0.006547066	0.790287854	0.351569707	0.741820873
		620		2.31E-08	455.1466123	0.302428592	10755.74148	1.54E-05	-2.31E-05	0.000811432	-1.05E-06	-0.034248382	0.002255968	0.960326631	0.33023527	0.48713626
		665		1.73E-08	324.1399454	0.211646624	9228.514298	1.83E-05	-2.47E-05	0.000863584	-7.52E-07	-0.029201677	0.001688924	0.973507465	0.312112489	0.41037945
		673.75	1.84E-08	309.5973272	0.200582425	9173.541823	1.92E-05	-2.47E-05	0.000866404	-7.51E-07	-0.028414686	0.00165914	0.974207723	0.308962967	0.398463364
		681.25	1.68E-08	277.0481248	0.178847726	8810.560228	1.97E-05	-2.56E-05	0.000896702	-7.63E-07	-0.027573797	0.001620068	0.974347474	0.310560364	0.402151799];

lbls = {"wv", 	"u2_Rrs", 	"u2_Lsky", 	"u2_Lu", 	"u2_Ed", 	"u2_rho", 	"dRrs2dLsky", "dRrs2dLu", "dRrs2dEd", "dRrs2drho", 	"1_Lu2Ed", 	"r_Lsky_Lu", "r_Lsky_Ed", "r_Lu_Ed"};



for id = 1:length(lbls)
	
	b.(lbls{id}) = d(:,id);
	
endfor



# compute different terms of error budget

b.bdgt.u2rrs_Lu = 	b.u2_Lu .* 		b.dRrs2dLu.^2; # Rrs uncertainty due to Lu
b.bdgt.u2rrs_Lsky = b.u2_Lsky .* 	b.dRrs2dLsky.^2; # Rrs uncertainty due to Lsky
b.bdgt.u2rrs_Ed = 	b.u2_Ed .* 		b.dRrs2dEd.^2; # Rrs uncertainty due to Ed
b.bdgt.u2rrs_rho = 	b.u2_rho .* 	b.dRrs2drho.^2; # Rrs uncertainty due to rho

b.bdgt.corr_Lu_Lsky = 	b.dRrs2dLu .* 	b.dRrs2dLsky .* sqrt(b.u2_Lu) .* 	sqrt(b.u2_Lsky) .* 	b.r_Lsky_Lu; 	# correlation term
b.bdgt.corr_Lu_Ed = 	b.dRrs2dLu .* 	b.dRrs2dEd .* 	sqrt(b.u2_Lu) .* 	sqrt(b.u2_Ed) .* 	b.r_Lu_Ed; 		# correlation term
b.bdgt.corr_Lsky_Ed = 	b.dRrs2dLsky .* b.dRrs2dEd .* 	sqrt(b.u2_Lsky) .* 	sqrt(b.u2_Ed) .* 	b.r_Lsky_Ed; 	# correlation term

b.bdgt.corr_Lu_rho = 	b.dRrs2dLu .* 	b.dRrs2drho .* 	sqrt(b.u2_Lu) .* 	sqrt(b.u2_rho) .* 	0; # correlation term
b.bdgt.corr_Lsky_rho = 	b.dRrs2dLsky .* b.dRrs2drho .* 	sqrt(b.u2_Lsky) .* 	sqrt(b.u2_rho) .* 	0; # correlation term
b.bdgt.corr_Ed_rho = 	b.dRrs2dEd .* 	b.dRrs2drho .* 	sqrt(b.u2_Ed) .* 	sqrt(b.u2_rho) .* 	0; # correlation term

f = fieldnames(b.bdgt);
icorr = strmatch('corr_', f);
iu2 = strmatch('u2rrs_', f);

b.bdgt.corr_all = zeros(size(b.wv));
for ic =1:length(icorr)
	f{icorr(ic)};
	b.bdgt.corr_all = b.bdgt.(f{icorr(ic)}) + b.bdgt.corr_all;
endfor

b.bdgt.u2_all = zeros(size(b.wv));
for ic =1:length(iu2)
	f{iu2(ic)};
	b.bdgt.u2_all = b.bdgt.(f{iu2(ic)}) + b.bdgt.u2_all;
endfor


clf
hold on
	plot(b.wv, b.bdgt.u2rrs_Lu, 	';Lu;', lw, 1)
	plot(b.wv, b.bdgt.u2rrs_Lsky, 	';Lsky;', lw, 1)
	plot(b.wv, b.bdgt.u2rrs_Ed, 	';Ed;', lw, 1)
	plot(b.wv, b.bdgt.u2rrs_rho, 	';rho;', lw, 1)
	plot(b.wv, b.bdgt.corr_all, 	';all correls;', lw, 1)
	plot(b.wv, b.bdgt.u2_all, 		'				;Rrs_{est};', lw, 4)
	plot(b.wv, b.bdgt.u2_all + b.bdgt.corr_all, '	;Rrs_{est} with correls;', lw, 4)
	plot(b.wv, b.u2_Rrs, '							;Rrs_{meas};', lw, 4)

	set(gca, fs, 20)


