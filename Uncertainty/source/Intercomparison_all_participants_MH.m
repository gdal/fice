% Evaluation of FRM4SOC Intercomparison-campaign at AAOT July 2018
% Martin Hieronymi, HZG
% 2019


clear
clc

%% Farbe

OLCI_VIS_Colors     = [400 112 32 147; 
                       412 153 63 166;
                       442 138 48 187;
                       490 0 129 187;
                       510 0 141 123;
                       560 102 190 73;
                       620 255 142 44;
                       665 204 102 0;
                       673 204 51 0;
                       681 214 51 51]; 
                   


% Comparison of Data of all participants 

%% Data import 

dir         = 'C:\Users\Hieronym\Desktop\FRM4SOC\FRM4SOC_participants_data\'; 

File_AWI    = 'FRM4SOC_FICE-AAOT_Data_Astrid_inair_NEW_EdSAM81EA.xlsx';
File_CIMA   = 'FRM4SOC_FICE-AAOT_Data_CIMA.xlsx';
File_LOV    = 'FRM4SOC_FICE-AAOT_Data_LOV.xlsx';
File_RBINS  = 'FRM4SOC_FICE-AAOT_Data_RBINS_v3.xlsx';
File_TO     = 'FRM4SOC_FICE-AAOT_Data_TO.xlsx';
File_UVIC   = 'FRM4SOC_FICE-AAOT_Data_UVIC.xlsx';
File_PML    = 'FRM4SOC_FICE-AAOT_PML_data.xlsx';


%% CIMA 

Data_CIMA   = readtable([dir, File_CIMA]); 

lambda_OLCI = table2array(Data_CIMA(10,8:26));

Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 

lines       = 13:3:115; 

for i = 1: length(lines)
    
    h           = table2array(Data_CIMA(lines(i),5)); 
    Wind(i,1)   = str2double(h{1,1});
    
end


rho_w_CIMA  = table2array(Data_CIMA(13:3:115,8:26));

rho_w_CIMA(rho_w_CIMA < -1) = NaN; 

% Rrs_CIMA    = rho_w_CIMA/ pi; 
Rrs_CIMA    = rho_w_CIMA; 

Ed_CIMA     = table2array(Data_CIMA(119:3:221,8:26));
Lu_CIMA     = table2array(Data_CIMA(225:3:327,8:26));
Lsky_CIMA   = table2array(Data_CIMA(331:3:433,8:26));


for i = 1: length(lambda_OLCI)
    rho_real_CIMA(:,i)   = (Lu_CIMA(:,i) - (Ed_CIMA(:,i).* Rrs_CIMA(:,i)))./ Lsky_CIMA(:,i); 
end


%% aus Stations und Wind notiert in CIMA (allen anderen auch)

rho_R06     = 0.0256 + 0.00039.* Wind + 0.000034.* Wind.^2; 

rho_MH      = load('C:\Users\Hieronym\Desktop\FRM4SOC\Daten\FRM4SOC_rho.txt');

rho_M99_MH  = rho_MH(Stations, 2);
rho_R06_MH  = rho_MH(Stations, 6);
rho_NIR_MH  = rho_MH(Stations, 7);





%% RBINS 

Data_RBINS  = readtable([dir, File_RBINS]); 

% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 

Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];

lines       = 1:35; 

for i = 1: 35
    
    h                           = table2array(Data_RBINS(lines(i),5)); 
    Wind_RBINS(Stations(i),1)   = str2double(h{1,1});
    
    Ed_RBINS(Stations(i),:)     = table2array(Data_RBINS(i,7:25));
    Lu_RBINS(Stations(i),:)     = table2array(Data_RBINS(i+210,7:25));
    Lsky_RBINS(Stations(i),:)   = table2array(Data_RBINS(i+105,7:25));
%     Rrs_RBINS(Stations(i),:)    = table2array(Data_RBINS(i+315,7:25));
    rho_w_RBINS(Stations(i),:)  = table2array(Data_RBINS(i+315,7:25));
    
end

Wind_RBINS(Wind_RBINS(:,1) == 0, :)     = []; 
Ed_RBINS(Ed_RBINS(:,1) == 0, :)         = []; 
Lu_RBINS(Lu_RBINS(:,1) == 0, :)         = []; 
Lsky_RBINS(Lsky_RBINS(:,1) == 0, :)     = []; 
% Rrs_RBINS(Rrs_RBINS(:,1) == 0, :)       = []; 
rho_w_RBINS(rho_w_RBINS(:,1) == 0, :)       = []; 

Rrs_RBINS    = rho_w_RBINS/ pi; 


for i = 1: length(lambda_OLCI)
    rho_real_RBINS(:,i)   = (Lu_RBINS(:,i) - (Ed_RBINS(:,i).* Rrs_RBINS(:,i)))./ Lsky_RBINS(:,i); 
end



%% TO 

Data_TO  = readtable([dir, File_TO]); 

% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29, 61, 62, 64, 66, 63, 65]; 


% lines       = 7:3:109; 
lines       = 6:3:108; 

for i = 1: 35
    
    h                           = table2array(Data_TO(lines(i),5)); 
    
    Wind_TO(Stations(i),1)   = str2double(h{1,1});
    
    Ed_TO(Stations(i),:)     = table2array(Data_TO(lines(i)+106,10:28));
    Lu_TO(Stations(i),:)     = table2array(Data_TO(lines(i)+212,10:28));
    Lsky_TO(Stations(i),:)   = table2array(Data_TO(lines(i)+318,10:28));
%     Rrs_TO(Stations(i),:)    = table2array(Data_TO(lines(i)+315,10:28));
    rho_w_TO(Stations(i),:)  = table2array(Data_TO(lines(i),10:28));
    
end

Wind_TO(Wind_TO(:,1) == 0, :)     = []; 
Ed_TO(Ed_TO(:,1) == 0, :)         = []; 
Lu_TO(Lu_TO(:,1) == 0, :)         = []; 
Lsky_TO(Lsky_TO(:,1) == 0, :)     = []; 
% Rrs_TO(Rrs_TO(:,1) == 0, :)       = []; 
rho_w_TO(rho_w_TO(:,1) == 0, :)       = []; 

Rrs_TO    = rho_w_TO/ pi; 


for i = 1: length(lambda_OLCI)
    rho_real_TO(:,i)   = (Lu_TO(:,i) - (Ed_TO(:,i).* Rrs_TO(:,i)))./ Lsky_TO(:,i); 
end



%% UVIC 

Data_UVIC  = readtable([dir, File_UVIC]); 

% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29, 61, 62, 64, 66, 63, 65]; 
Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29]; 


lines       = 13:3:97; 

for i = 1: length(Stations)
    
    h                           = table2array(Data_UVIC(lines(i),5)); 
    
    Wind_UVIC(Stations(i),1)   = str2double(h{1,1});
    
    Ed_UVIC(Stations(i),:)     = table2array(Data_UVIC(lines(i)+88,7:25));
    Lu_UVIC(Stations(i),:)     = table2array(Data_UVIC(lines(i)+176,7:25));
    Lsky_UVIC(Stations(i),:)   = table2array(Data_UVIC(lines(i)+264,7:25));
%     Rrs_UVIC(Stations(i),:)    = table2array(Data_UVIC(lines(i)+315,7:25));
    rho_w_UVIC(Stations(i),:)  = table2array(Data_UVIC(lines(i),7:25));
    
end

Wind_UVIC(30:35, :)     = NaN; 
Ed_UVIC(30:35, :)     = NaN; 
Lu_UVIC(30:35, :)     = NaN; 
Lsky_UVIC(30:35, :)     = NaN; 
% Rrs_UVIC(30:35, :)     = NaN; 
rho_w_UVIC(30:35, :)     = NaN; 

Rrs_UVIC    = rho_w_UVIC/ pi; 

% HyperSAS data with one magnitude difference 28.02.2019

Ed_UVIC         = 10* Ed_UVIC; 
Lu_UVIC         = 10* Lu_UVIC; 
Lsky_UVIC       = 10* Lsky_UVIC; 


for i = 1: length(lambda_OLCI)
    rho_real_UVIC(:,i)   = (Lu_UVIC(:,i) - (Ed_UVIC(:,i).* Rrs_UVIC(:,i)))./ Lsky_UVIC(:,i); 
end



%% PML 

Data_PML  = readtable([dir, File_PML]); 

% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29, 61, 62, 64, 66, 63, 65]; 
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29]; 

Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
    

lines       = 1:35; 

for i = 1: length(Stations)
    
    h                           = table2array(Data_PML(lines(i),5)); 
    
    Wind_PML(Stations(i),1)   = h;
    
    Ed_PML(Stations(i),:)     = table2array(Data_PML(lines(i),9:27));
    Lu_PML(Stations(i),:)     = table2array(Data_PML(lines(i)+140,9:27));
    Lsky_PML(Stations(i),:)   = table2array(Data_PML(lines(i)+70,9:27));
    rho_w_PML(Stations(i),:)  = table2array(Data_PML(lines(i)+210,9:27));
    
end

Wind_PML(Wind_PML(:,1) == 0, :)     = []; 
Ed_PML(Ed_PML(:,1) == 0, :)         = []; 
Lu_PML(Lu_PML(:,1) == 0, :)         = []; 
Lsky_PML(Lsky_PML(:,1) == 0, :)     = []; 
rho_w_PML(rho_w_PML(:,1) == 0, :)   = []; 

Rrs_PML    = rho_w_PML; 


% HyperSAS data with one magnitude difference 28.02.2019

Ed_PML         = 10* Ed_PML; 
Lu_PML         = 10* Lu_PML; 
Lsky_PML       = 10* Lsky_PML; 



for i = 1: length(lambda_OLCI)
    rho_real_PML(:,i)   = (Lu_PML(:,i) - (Ed_PML(:,i).* Rrs_PML(:,i)))./ Lsky_PML(:,i); 
end




%% AWI 

Data_AWI  = readtable([dir, File_AWI]); 

% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29, 61, 62, 64, 66, 63, 65]; 
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
    
Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 


lines       = 11:45; 
lines_2     = 52:3:154; 

Ed_AWI      = NaN(66,19); 
Lu_AWI      = NaN(66,19); 
Lsky_AWI    = NaN(66,19); 
rho_w_AWI   = NaN(66,19); 

for i = 1: length(Stations)
    
    h                         = table2array(Data_AWI(lines(i),5)); 
    
    Wind_AWI(Stations(i),1)   = str2double(h{1,1});
        
    Ed_AWI(Stations(i),[1:12,16:18])     = table2array(Data_AWI(lines_2(i),7:21));
    Lu_AWI(Stations(i),[1:12,16:18])     = table2array(Data_AWI(lines_2(i)+115,7:21));
    Lsky_AWI(Stations(i),[1:12,16:18])   = table2array(Data_AWI(lines_2(i)+230,7:21));
    rho_w_AWI(Stations(i),[1:12,16:18])  = table2array(Data_AWI(lines(i),7:21));
    
end

Wind_AWI(30:60, :)      = []; 
Ed_AWI(30:60, :)        = []; 
Lu_AWI(30:60, :)        = []; 
Lsky_AWI(30:60, :)      = []; 
rho_w_AWI(30:60, :)     = []; 



Rrs_AWI    = rho_w_AWI/pi; 


for i = 1: length(lambda_OLCI)
    rho_real_AWI(:,i)   = (Lu_AWI(:,i) - (Ed_AWI(:,i).* Rrs_AWI(:,i)))./ Lsky_AWI(:,i); 
end


%% LOV 

Data_LOV  = readtable([dir, File_LOV]); 

% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29, 61, 62, 64, 66, 63, 65]; 
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 21, 23, 24, 25, 26, 27, 28, 29]; 
% Stations    = [1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 3, 4, 5, 6, 61, 62, 63, 64, 65, 66, 7, 8, 9];  
% Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 

Stations    = [1, 2, 3, 4, 5, 6, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 24, 25, 26, 27, 28, 29, 62, 63, 64, 65, 66]; 

lambda_LOV      = table2array(Data_LOV(10,5:15));

% LOV's wavelengths +/- 5 nm to match OLCI bands 
lambda_LOV_mod  = [400 412.5 442.5 490 NaN 560 620 665 NaN 681.25 708.75 NaN NaN NaN NaN 778.75 NaN NaN NaN];
columns_LOV_mod = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11];     % 555 nm not used 

lines       = 13:3:91;

for i = 1: length(Stations)
    
        
%     Ed_LOV(Stations(i),[1:12,16:18])     = table2array(Data_LOV(lines_2(i),7:21));
%     Lu_LOV(Stations(i),[1:12,16:18])     = table2array(Data_LOV(lines_2(i)+115,7:21));
%     Lsky_LOV(Stations(i),[1:12,16:18])   = table2array(Data_LOV(lines_2(i)+230,7:21));
    rho_w_LOV(Stations(i),:)  = table2array(Data_LOV(lines(i),5:15));
    
end

% Wind_LOV(30:60, :)      = []; 
% Ed_LOV(30:60, :)        = []; 
% Lu_LOV(30:60, :)        = []; 
% Lsky_LOV(30:60, :)      = []; 
rho_w_LOV(30:60, :)         = []; 
rho_w_LOV(rho_w_LOV == 0)   = NaN; 


Rrs_LOV    = rho_w_LOV/pi; 





%% HZG

Stations    = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 61, 62, 63, 64, 65, 66]; 


dir         = 'C:\Users\Hieronym\Desktop\FRM4SOC\Daten\Deliver\';

Data        = load([dir, 'FRM4SOC_HZG_Ed_mean_SRF_OLCI.txt']);
Ed_HZG      = Data(1+Stations, 2:end); 
Data        = load([dir, 'FRM4SOC_HZG_Lsky_mean_SRF_OLCI.txt']);    
Lsky_HZG    = Data(1+Stations, 2:end); 
Data        = load([dir, 'FRM4SOC_HZG_Lu_mean_SRF_OLCI.txt']);      
Lu_HZG      = Data(1+Stations, 2:end); 
Data        = load([dir, 'FRM4SOC_HZG_Rrs_M99_2_SRF_OLCI.txt']);    
Rrs_HZG     = Data(1+Stations, 2:end); 


for i = 1: length(lambda_OLCI)
    rho_real_HZG(:,i)   = (Lu_HZG(:,i) - (Ed_HZG(:,i).* Rrs_HZG(:,i)))./ Lsky_HZG(:,i); 
end




%%  All calculated with same rho 

Rrs_HZG_R06     = (Lu_HZG - rho_R06.*Lsky_HZG)./Ed_HZG; 
Rrs_CIMA_R06    = (Lu_CIMA - rho_R06.*Lsky_CIMA)./Ed_CIMA; 
Rrs_RBINS_R06   = (Lu_RBINS - rho_R06.*Lsky_RBINS)./Ed_RBINS; 
Rrs_TO_R06      = (Lu_TO - rho_R06.*Lsky_TO)./Ed_TO; 
Rrs_UVIC_R06    = (Lu_UVIC - rho_R06.*Lsky_UVIC)./Ed_UVIC; 
Rrs_PML_R06     = (Lu_PML - rho_R06.*Lsky_PML)./Ed_PML; 
Rrs_AWI_R06     = (Lu_AWI - rho_R06.*Lsky_AWI)./Ed_AWI; 

Rrs_HZG_M99_MH     = (Lu_HZG - rho_M99_MH.*Lsky_HZG)./Ed_HZG; 
Rrs_CIMA_M99_MH    = (Lu_CIMA - rho_M99_MH.*Lsky_CIMA)./Ed_CIMA; 
Rrs_RBINS_M99_MH   = (Lu_RBINS - rho_M99_MH.*Lsky_RBINS)./Ed_RBINS; 
Rrs_TO_M99_MH      = (Lu_TO - rho_M99_MH.*Lsky_TO)./Ed_TO; 
Rrs_UVIC_M99_MH    = (Lu_UVIC - rho_M99_MH.*Lsky_UVIC)./Ed_UVIC; 
Rrs_PML_M99_MH     = (Lu_PML - rho_M99_MH.*Lsky_PML)./Ed_PML; 
Rrs_AWI_M99_MH     = (Lu_AWI - rho_M99_MH.*Lsky_AWI)./Ed_AWI; 

Rrs_LOV_mod     = NaN(size(Rrs_HZG_R06)); 

Rrs_LOV_mod(:, ~isnan(lambda_LOV_mod)) = Rrs_LOV(:,columns_LOV_mod); 


Rrs_CIMA_R06(1,:) = NaN;
Rrs_CIMA_M99_MH(1,:) = NaN;

for i = 1: 35
    for j = 1: 19
        
%         Rrs_mean_R06(i,j)   = (Rrs_HZG_R06(i,j) + Rrs_CIMA_R06(i,j) + Rrs_RBINS_R06(i,j) + Rrs_TO_R06(i,j) + Rrs_UVIC_R06(i,j) + Rrs_PML_R06(i,j) + Rrs_AWI_R06(i,j))./7;
%         Rrs_mean(i,j)       = (Rrs_HZG(i,j) + Rrs_CIMA(i,j) + Rrs_RBINS(i,j) + Rrs_TO(i,j) + Rrs_UVIC(i,j) + Rrs_PML(i,j) + Rrs_AWI(i,j))./7;
%     
%         Rrs_std_R06(i,j)    = std([Rrs_HZG_R06(i,j), Rrs_CIMA_R06(i,j), Rrs_RBINS_R06(i,j), Rrs_TO_R06(i,j), Rrs_UVIC_R06(i,j), Rrs_PML_R06(i,j), Rrs_AWI_R06(i,j)]);
%         Rrs_std(i,j)        = std([Rrs_HZG(i,j), Rrs_CIMA(i,j), Rrs_RBINS(i,j), Rrs_TO(i,j), Rrs_UVIC(i,j), Rrs_PML(i,j), Rrs_AWI(i,j)]);
% 
%         CV_mean_R06(i,j)    = 100* Rrs_std_R06(i,j)./ Rrs_mean_R06(i,j); 
%         CV_mean(i,j)        = 100* Rrs_std(i,j)./ Rrs_mean(i,j); 
        
        A                   = [Rrs_HZG_R06(i,j) Rrs_CIMA_R06(i,j) Rrs_RBINS_R06(i,j) Rrs_TO_R06(i,j) Rrs_UVIC_R06(i,j) Rrs_PML_R06(i,j) Rrs_AWI_R06(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean_R06(i,j)   = sum(A)./B;
        Rrs_std_R06(i,j)    = std(A);
        CV_mean_R06(i,j)    = 100* Rrs_std_R06(i,j)./ Rrs_mean_R06(i,j); 
        
        
        A                   = [Rrs_HZG_M99_MH(i,j) Rrs_CIMA_M99_MH(i,j) Rrs_RBINS_M99_MH(i,j) Rrs_TO_M99_MH(i,j) Rrs_UVIC_M99_MH(i,j) Rrs_PML_M99_MH(i,j) Rrs_AWI_M99_MH(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean_M99_MH(i,j)   = sum(A)./B;
        Rrs_std_M99_MH(i,j)    = std(A);
        CV_mean_M99_MH(i,j)    = 100* Rrs_std_M99_MH(i,j)./ Rrs_mean_M99_MH(i,j); 
        
        A                   = [Rrs_HZG(i,j) Rrs_CIMA(i,j) Rrs_RBINS(i,j) Rrs_TO(i,j) Rrs_UVIC(i,j) Rrs_PML(i,j) Rrs_AWI(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean(i,j)       = sum(A)./B;
        Rrs_std(i,j)        = std(A);
        CV_mean(i,j)        = 100* Rrs_std(i,j)./ Rrs_mean(i,j); 
        
        % Including modified LOV data 
        A                   = [Rrs_LOV_mod(i,j) Rrs_HZG(i,j) Rrs_CIMA(i,j) Rrs_RBINS(i,j) Rrs_TO(i,j) Rrs_UVIC(i,j) Rrs_PML(i,j) Rrs_AWI(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean_all(i,j)       = sum(A)./B;
        Rrs_std_all(i,j)        = std(A);
        CV_mean_all(i,j)        = 100* Rrs_std_all(i,j)./ Rrs_mean_all(i,j); 
        

        
    end
end


Reduction_in_CV             = 100* (CV_mean_M99_MH - CV_mean)./CV_mean;
Reduction_in_CV(14:15,:)    = NaN;


%%  CV change due to Ed, Lsky and Lt - after correspondence with Gavin 28.08.2019

Rrs_HZG_EdHZG_MH     = (Lu_HZG - rho_real_HZG.*Lsky_HZG)./Ed_HZG; 
Rrs_CIMA_EdHZG_MH    = (Lu_CIMA - rho_real_CIMA.*Lsky_CIMA)./Ed_HZG; 
Rrs_RBINS_EdHZG_MH   = (Lu_RBINS - rho_real_RBINS.*Lsky_RBINS)./Ed_HZG; 
Rrs_TO_EdHZG_MH      = (Lu_TO - rho_real_TO.*Lsky_TO)./Ed_HZG; 
Rrs_UVIC_EdHZG_MH    = (Lu_UVIC - rho_real_UVIC.*Lsky_UVIC)./Ed_HZG; 
Rrs_PML_EdHZG_MH     = (Lu_PML - rho_real_PML.*Lsky_PML)./Ed_HZG; 
Rrs_AWI_EdHZG_MH     = (Lu_AWI - rho_real_AWI.*Lsky_AWI)./Ed_HZG; 

Rrs_HZG_LskyHZG_MH     = (Lu_HZG - rho_real_HZG.*Lsky_HZG)./Ed_HZG; 
Rrs_CIMA_LskyHZG_MH    = (Lu_CIMA - rho_real_CIMA.*Lsky_HZG)./Ed_CIMA; 
Rrs_RBINS_LskyHZG_MH   = (Lu_RBINS - rho_real_RBINS.*Lsky_HZG)./Ed_RBINS; 
Rrs_TO_LskyHZG_MH      = (Lu_TO - rho_real_TO.*Lsky_HZG)./Ed_TO; 
Rrs_UVIC_LskyHZG_MH    = (Lu_UVIC - rho_real_UVIC.*Lsky_HZG)./Ed_UVIC; 
Rrs_PML_LskyHZG_MH     = (Lu_PML - rho_real_PML.*Lsky_HZG)./Ed_PML; 
Rrs_AWI_LskyHZG_MH     = (Lu_AWI - rho_real_AWI.*Lsky_HZG)./Ed_AWI; 

Rrs_HZG_LuHZG_MH     = (Lu_HZG - rho_real_HZG.*Lsky_HZG)./Ed_HZG; 
Rrs_CIMA_LuHZG_MH    = (Lu_HZG - rho_real_CIMA.*Lsky_CIMA)./Ed_CIMA; 
Rrs_RBINS_LuHZG_MH   = (Lu_HZG - rho_real_RBINS.*Lsky_RBINS)./Ed_RBINS; 
Rrs_TO_LuHZG_MH      = (Lu_HZG - rho_real_TO.*Lsky_TO)./Ed_TO; 
Rrs_UVIC_LuHZG_MH    = (Lu_HZG - rho_real_UVIC.*Lsky_UVIC)./Ed_UVIC; 
Rrs_PML_LuHZG_MH     = (Lu_HZG - rho_real_PML.*Lsky_PML)./Ed_PML; 
Rrs_AWI_LuHZG_MH     = (Lu_HZG - rho_real_AWI.*Lsky_AWI)./Ed_AWI; 

% Rrs_HZG_EdHZG_MH     = (Lu_HZG - rho_M99_MH.*Lsky_HZG)./Ed_HZG; 
% Rrs_CIMA_EdHZG_MH    = (Lu_CIMA - rho_M99_MH.*Lsky_CIMA)./Ed_HZG; 
% Rrs_RBINS_EdHZG_MH   = (Lu_RBINS - rho_M99_MH.*Lsky_RBINS)./Ed_HZG; 
% Rrs_TO_EdHZG_MH      = (Lu_TO - rho_M99_MH.*Lsky_TO)./Ed_HZG; 
% Rrs_UVIC_EdHZG_MH    = (Lu_UVIC - rho_M99_MH.*Lsky_UVIC)./Ed_HZG; 
% Rrs_PML_EdHZG_MH     = (Lu_PML - rho_M99_MH.*Lsky_PML)./Ed_HZG; 
% Rrs_AWI_EdHZG_MH     = (Lu_AWI - rho_M99_MH.*Lsky_AWI)./Ed_HZG; 
% 
% Rrs_HZG_LskyHZG_MH     = (Lu_HZG - rho_M99_MH.*Lsky_HZG)./Ed_HZG; 
% Rrs_CIMA_LskyHZG_MH    = (Lu_CIMA - rho_M99_MH.*Lsky_HZG)./Ed_CIMA; 
% Rrs_RBINS_LskyHZG_MH   = (Lu_RBINS - rho_M99_MH.*Lsky_HZG)./Ed_RBINS; 
% Rrs_TO_LskyHZG_MH      = (Lu_TO - rho_M99_MH.*Lsky_HZG)./Ed_TO; 
% Rrs_UVIC_LskyHZG_MH    = (Lu_UVIC - rho_M99_MH.*Lsky_HZG)./Ed_UVIC; 
% Rrs_PML_LskyHZG_MH     = (Lu_PML - rho_M99_MH.*Lsky_HZG)./Ed_PML; 
% Rrs_AWI_LskyHZG_MH     = (Lu_AWI - rho_M99_MH.*Lsky_HZG)./Ed_AWI; 
% 
% Rrs_HZG_LuHZG_MH     = (Lu_HZG - rho_M99_MH.*Lsky_HZG)./Ed_HZG; 
% Rrs_CIMA_LuHZG_MH    = (Lu_HZG - rho_M99_MH.*Lsky_CIMA)./Ed_CIMA; 
% Rrs_RBINS_LuHZG_MH   = (Lu_HZG - rho_M99_MH.*Lsky_RBINS)./Ed_RBINS; 
% Rrs_TO_LuHZG_MH      = (Lu_HZG - rho_M99_MH.*Lsky_TO)./Ed_TO; 
% Rrs_UVIC_LuHZG_MH    = (Lu_HZG - rho_M99_MH.*Lsky_UVIC)./Ed_UVIC; 
% Rrs_PML_LuHZG_MH     = (Lu_HZG - rho_M99_MH.*Lsky_PML)./Ed_PML; 
% Rrs_AWI_LuHZG_MH     = (Lu_HZG - rho_M99_MH.*Lsky_AWI)./Ed_AWI; 



for i = 1: 35
    for j = 1: 19
                
        A                   = [Rrs_HZG_EdHZG_MH(i,j) Rrs_CIMA_EdHZG_MH(i,j) Rrs_RBINS_EdHZG_MH(i,j) Rrs_TO_EdHZG_MH(i,j) Rrs_UVIC_EdHZG_MH(i,j) Rrs_PML_EdHZG_MH(i,j) Rrs_AWI_EdHZG_MH(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean_EdHZG_MH(i,j)   = sum(A)./B;
        Rrs_std_EdHZG_MH(i,j)    = std(A);
        CV_mean_EdHZG_MH(i,j)    = 100* Rrs_std_EdHZG_MH(i,j)./ Rrs_mean_EdHZG_MH(i,j); 
        
        A                   = [Rrs_HZG_LuHZG_MH(i,j) Rrs_CIMA_LuHZG_MH(i,j) Rrs_RBINS_LuHZG_MH(i,j) Rrs_TO_LuHZG_MH(i,j) Rrs_UVIC_LuHZG_MH(i,j) Rrs_PML_LuHZG_MH(i,j) Rrs_AWI_LuHZG_MH(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean_LuHZG_MH(i,j)   = sum(A)./B;
        Rrs_std_LuHZG_MH(i,j)    = std(A);
        CV_mean_LuHZG_MH(i,j)    = 100* Rrs_std_LuHZG_MH(i,j)./ Rrs_mean_LuHZG_MH(i,j); 
        
        A                   = [Rrs_HZG_LskyHZG_MH(i,j) Rrs_CIMA_LskyHZG_MH(i,j) Rrs_RBINS_LskyHZG_MH(i,j) Rrs_TO_LskyHZG_MH(i,j) Rrs_UVIC_LskyHZG_MH(i,j) Rrs_PML_LskyHZG_MH(i,j) Rrs_AWI_LskyHZG_MH(i,j)];
        B                   = sum(~isnan(A)); 
        A                   = A(~isnan(A));
        
        Rrs_mean_LskyHZG_MH(i,j)   = sum(A)./B;
        Rrs_std_LskyHZG_MH(i,j)    = std(A);
        CV_mean_LskyHZG_MH(i,j)    = 100* Rrs_std_LskyHZG_MH(i,j)./ Rrs_mean_LskyHZG_MH(i,j); 
        
        
    end
end


Reduction_in_CV_EdHZG               = 100* (CV_mean_EdHZG_MH - CV_mean)./CV_mean;
Reduction_in_CV_EdHZG(14:15,:)      = NaN;

Reduction_in_CV_LuHZG               = 100* (CV_mean_LuHZG_MH - CV_mean)./CV_mean;
Reduction_in_CV_LuHZG(14:15,:)      = NaN;

Reduction_in_CV_LskyHZG             = 100* (CV_mean_LskyHZG_MH - CV_mean)./CV_mean;
Reduction_in_CV_LskyHZG(14:15,:)    = NaN;





%%

Stations_select_Nr      = [2:13, 16:35]; 
Stations_select         = Stations(Stations_select_Nr)'; 

Viewing     = load('C:\Users\Hieronym\Desktop\FRM4SOC\Daten\FRM4SOC_sensor_azimuth.txt'); 

Viewing_select          = Viewing(Stations_select, 2); 



%% Propagation of uncertainties Email by Gavin 20190830

% C.      We could then apply the law of propagation of uncertainty (GUM) to this analysis. To do this, we would need the following quantities computed over all (clear sky) stations and groups (participants) as a function of wavelength:
% -          Variance of Rrs; this is the variance for each wavelength computed for all stations and all groups,
% -          Variance of Lsky; again computed for each wavelength over all stations and all groups,
% -          Variance of Lt; as above,
% -          Variance of Ed; as above,
% -          Variance of Rho;  as above,
% -          Average value of Rho/Ed (please note this is NOT average Rho/Average Ed),
% -          Average value of 1/Ed  (please note this is NOT 1/Average Ed),
% -          Average value of Rrs/Ed  (please note this is NOT average Rrs/Average Ed),
% -          Average value of Lsky/Ed (please note this is NOT average Lsky/Average Ed),
% -          Correlation coefficient between Lsky and Lt,
% -          Correlation coefficient between Lsky and Ed,
% -          Correlation coefficient between Lt and Ed,



i = Stations_select_Nr; 

for j = 1: 10
                
        A                       = [Rrs_HZG(i,j) Rrs_CIMA(i,j) Rrs_RBINS(i,j) Rrs_TO(i,j) Rrs_UVIC(i,j) Rrs_PML(i,j) Rrs_AWI(i,j)];
        B                       = A(~isnan(A));
        
        Rrs                     = B; 

        Variance_of_Rrs(j)      = var(B);
        
        A                       = [Ed_HZG(i,j) Ed_CIMA(i,j) Ed_RBINS(i,j) Ed_TO(i,j) Ed_UVIC(i,j) Ed_PML(i,j) Ed_AWI(i,j)];
        B                       = A(~isnan(A));
        
        Ed                      = B; 

        Variance_of_Ed(j)       = var(B);
        
        A                       = [Lsky_HZG(i,j) Lsky_CIMA(i,j) Lsky_RBINS(i,j) Lsky_TO(i,j) Lsky_UVIC(i,j) Lsky_PML(i,j) Lsky_AWI(i,j)];
        B                       = A(~isnan(A));
        
        Lsky                    = B; 

        Variance_of_Lsky(j)     = var(B);
        
        A                       = [Lu_HZG(i,j) Lu_CIMA(i,j) Lu_RBINS(i,j) Lu_TO(i,j) Lu_UVIC(i,j) Lu_PML(i,j) Lu_AWI(i,j)];
        B                       = A(~isnan(A));
        
        Lu                      = B; 

        Variance_of_Lu(j)       = var(B);
        
        A                       = [rho_real_HZG(i,j) rho_real_CIMA(i,j) rho_real_RBINS(i,j) rho_real_TO(i,j) rho_real_UVIC(i,j) rho_real_PML(i,j) rho_real_AWI(i,j)];
        B                       = A(~isnan(A));
        
        Rho                     = B; 

        Variance_of_rho_real(j) = var(B);
        
        
        Average_value_Rho_Ed(j)    = mean(Rho./Ed); 
        Average_value_1_Ed(j)      = mean(1./Ed); 
        Average_value_Rrs_Ed(j)    = mean(Rrs./Ed); 
        Average_value_Lsky_Ed(j)   = mean(Lsky./Ed); 
        Average_value_Lu_Ed(j)     = mean(Lu./Ed); 
        
        [r,p]                       = corrcoef(Lsky, Lu);
        Correlation_Coeff_Lsky_Lu(j)= r(2);
        
        [r,p]                       = corrcoef(Lsky, Ed);
        Correlation_Coeff_Lsky_Ed(j)= r(2);
        
        [r,p]                       = corrcoef(Lu, Ed);
        Correlation_Coeff_Lu_Ed(j)  = r(2);
        
        


        
end



%%  Inclinations  

lambda_OLCI_diff    = diff(lambda_OLCI);
lambda_OLCI2        = lambda_OLCI(1:18)+lambda_OLCI_diff./2;
lambda_LOV2         = lambda_LOV(1:10)+diff(lambda_LOV)./2;

Rrs_AWI_1st         = NaN(35,18); 
Rrs_PML_1st         = NaN(35,18); 
Rrs_TO_1st          = NaN(35,18); 
Rrs_CIMA_1st        = NaN(35,18); 
Rrs_RBINS_1st       = NaN(35,18); 
Rrs_HZG_1st         = NaN(35,18); 
Rrs_UVIC_1st        = NaN(35,18); 
Rrs_LOV_1st         = NaN(35,10); 

Rrs_AWI_R06_1st         = NaN(35,18); 
Rrs_PML_R06_1st         = NaN(35,18); 
Rrs_TO_R06_1st          = NaN(35,18); 
Rrs_CIMA_R06_1st        = NaN(35,18); 
Rrs_RBINS_R06_1st       = NaN(35,18); 
Rrs_HZG_R06_1st         = NaN(35,18); 
Rrs_UVIC_R06_1st        = NaN(35,18); 

BandRatio_AWI       = NaN(35,3);
BandRatio_PML       = NaN(35,3);
BandRatio_TO        = NaN(35,3);
BandRatio_CIMA      = NaN(35,3);
BandRatio_RBINS     = NaN(35,3);
BandRatio_HZG       = NaN(35,3);
BandRatio_UVIC      = NaN(35,3);
BandRatio_LOV       = NaN(35,3);


for i = 1: 35

    Rrs_AWI_1st(i,:)    = diff(Rrs_AWI(i,:))./lambda_OLCI_diff; 
    Rrs_PML_1st(i,:)    = diff(Rrs_PML(i,:))./lambda_OLCI_diff; 
    Rrs_TO_1st(i,:)     = diff(Rrs_TO(i,:))./lambda_OLCI_diff; 
    Rrs_CIMA_1st(i,:)   = diff(Rrs_CIMA(i,:))./lambda_OLCI_diff; 
    Rrs_RBINS_1st(i,:)  = diff(Rrs_RBINS(i,:))./lambda_OLCI_diff; 
    Rrs_HZG_1st(i,:)    = diff(Rrs_HZG(i,:))./lambda_OLCI_diff; 
    Rrs_UVIC_1st(i,:)   = diff(Rrs_UVIC(i,:))./lambda_OLCI_diff; 
    Rrs_LOV_1st(i,:)    = diff(Rrs_LOV(i,:))./diff(lambda_LOV); 

    Rrs_AWI_R06_1st(i,:)    = diff(Rrs_AWI_R06(i,:))./lambda_OLCI_diff; 
    Rrs_PML_R06_1st(i,:)    = diff(Rrs_PML_R06(i,:))./lambda_OLCI_diff; 
    Rrs_TO_R06_1st(i,:)     = diff(Rrs_TO_R06(i,:))./lambda_OLCI_diff; 
    Rrs_CIMA_R06_1st(i,:)   = diff(Rrs_CIMA_R06(i,:))./lambda_OLCI_diff; 
    Rrs_RBINS_R06_1st(i,:)  = diff(Rrs_RBINS_R06(i,:))./lambda_OLCI_diff; 
    Rrs_HZG_R06_1st(i,:)    = diff(Rrs_HZG_R06(i,:))./lambda_OLCI_diff; 
    Rrs_UVIC_R06_1st(i,:)   = diff(Rrs_UVIC_R06(i,:))./lambda_OLCI_diff; 
    
    BandRatio_AWI(i,:)      = [Rrs_AWI(i,3)/Rrs_AWI(i,6), Rrs_AWI(i,4)/Rrs_AWI(i,6), Rrs_AWI(i,5)/Rrs_AWI(i,6)];
    BandRatio_PML(i,:)      = [Rrs_PML(i,3)/Rrs_PML(i,6), Rrs_PML(i,4)/Rrs_PML(i,6), Rrs_PML(i,5)/Rrs_PML(i,6)];
    BandRatio_TO(i,:)       = [Rrs_TO(i,3)/Rrs_TO(i,6), Rrs_TO(i,4)/Rrs_TO(i,6), Rrs_TO(i,5)/Rrs_TO(i,6)];
    BandRatio_CIMA(i,:)     = [Rrs_CIMA(i,3)/Rrs_CIMA(i,6), Rrs_CIMA(i,4)/Rrs_CIMA(i,6), Rrs_CIMA(i,5)/Rrs_CIMA(i,6)];
    BandRatio_RBINS(i,:)    = [Rrs_RBINS(i,3)/Rrs_RBINS(i,6), Rrs_RBINS(i,4)/Rrs_RBINS(i,6), Rrs_RBINS(i,5)/Rrs_RBINS(i,6)];
    BandRatio_HZG(i,:)      = [Rrs_HZG(i,3)/Rrs_HZG(i,6), Rrs_HZG(i,4)/Rrs_HZG(i,6), Rrs_HZG(i,5)/Rrs_HZG(i,6)];
    BandRatio_UVIC(i,:)     = [Rrs_UVIC(i,3)/Rrs_UVIC(i,6), Rrs_UVIC(i,4)/Rrs_UVIC(i,6), Rrs_UVIC(i,5)/Rrs_UVIC(i,6)];
    BandRatio_LOV(i,:)      = [Rrs_LOV(i,3)/Rrs_LOV(i,6), Rrs_LOV(i,4)/Rrs_LOV(i,6), Rrs_LOV(i,5)/Rrs_LOV(i,6)];
    

end 




%%  Save files

% % % % CV_mean
% % % % CV_mean_M99_MH
% % % % Rrs_AWI
% % % % Rrs_AWI_1st
% % % % Rrs_AWI_M99_MH
% % % 
% % % for i = 1: 7
% % %     
% % %     if i == 1
% % %         Team    = 'AWI'; 
% % %         R       = Rrs_AWI; 
% % %         R1      = Rrs_AWI_1st;
% % %         R2      = Rrs_AWI_M99_MH; 
% % %         R3      = rho_real_AWI; 
% % %     elseif i == 2
% % %         Team    = 'CIMA';
% % %         R       = Rrs_CIMA; 
% % %         R1      = Rrs_CIMA_1st;
% % %         R2      = Rrs_CIMA_M99_MH; 
% % %         R3      = rho_real_CIMA; 
% % %     elseif i == 3
% % %         Team    = 'HZG';
% % %         R       = Rrs_HZG; 
% % %         R1      = Rrs_HZG_1st;
% % %         R2      = Rrs_HZG_M99_MH; 
% % %         R3      = rho_real_HZG; 
% % %     elseif i == 4
% % %         Team    = 'PML';
% % %         R       = Rrs_PML; 
% % %         R1      = Rrs_PML_1st;
% % %         R2      = Rrs_PML_M99_MH; 
% % %         R3      = rho_real_PML; 
% % %     elseif i == 5
% % %         Team    = 'RBINS'; 
% % %         R       = Rrs_RBINS; 
% % %         R1      = Rrs_RBINS_1st;
% % %         R2      = Rrs_RBINS_M99_MH; 
% % %         R3      = rho_real_RBINS; 
% % %     elseif i == 6
% % %         Team    = 'TO';
% % %         R       = Rrs_TO; 
% % %         R1      = Rrs_TO_1st;
% % %         R2      = Rrs_TO_M99_MH; 
% % %         R3      = rho_real_TO; 
% % %     elseif i == 7 
% % %         Team    = 'UVIC'; 
% % %         R       = Rrs_UVIC; 
% % %         R1      = Rrs_UVIC_1st;
% % %         R2      = Rrs_UVIC_M99_MH; 
% % %         R3      = rho_real_UVIC; 
% % %     end
% % %     
% % % %     % CV_mean 
% % % %     
% % % %     File        = ['FRM4SOC_2018_', Team, '_CV.txt'];
% % % %     
% % % %     clear Data 
% % % %     
% % % %     Data(1,:)       = [0 lambda_OLCI]; 
% % % %     Data(2:36,1)    = Stations;
% % % %     Data(2:36,2:20) = CV_mean; 
% % % %     
% % % %     fileID          = fopen(File, 'w+');
% % % % 
% % % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % % %     fprintf(fileID, '%3.0f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\r\n', Data(2:end,:)'); 
% % % % 
% % % %     fclose(fileID);
% % % % 
% % % % 
% % % %     % CV_mean_M99_MH 
% % % %     
% % % %     File        = ['FRM4SOC_2018_', Team, '_CV_M99.txt'];
% % % %     
% % % %     clear Data 
% % % %     
% % % %     Data(1,:)       = [0 lambda_OLCI]; 
% % % %     Data(2:36,1)    = Stations;
% % % %     Data(2:36,2:20) = CV_mean_M99_MH; 
% % % %     
% % % %     fileID          = fopen(File, 'w+');
% % % % 
% % % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % % %     fprintf(fileID, '%3.0f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\r\n', Data(2:end,:)'); 
% % % % 
% % % %     fclose(fileID);
% % % % 
% % % % 
% % % %     % Rrs_AWI 
% % % %     
% % % %     File        = ['FRM4SOC_2018_', Team, '_Rrs.txt'];
% % % %     
% % % %     clear Data 
% % % %     
% % % %     Data(1,:)       = [0 lambda_OLCI]; 
% % % %     Data(2:36,1)    = Stations;
% % % %     Data(2:36,2:20) = R; 
% % % %     
% % % %     fileID          = fopen(File, 'w+');
% % % % 
% % % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % % %     fprintf(fileID, '%3.0f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f\r\n', Data(2:end,:)'); 
% % % % 
% % % %     fclose(fileID);
% % % % 
% % % % 
% % % %     % Rrs_CIMA_1st 
% % % %     
% % % %     File        = ['FRM4SOC_2018_', Team, '_Rrs_1stdev.txt'];
% % % %     
% % % %     clear Data 
% % % %     
% % % %     Data(1,:)       = [0 lambda_OLCI2]; 
% % % %     Data(2:36,1)    = Stations;
% % % %     Data(2:36,2:19) = R1; 
% % % %     
% % % %     fileID          = fopen(File, 'w+');
% % % % 
% % % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % % %     fprintf(fileID, '%3.0f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f\r\n', Data(2:end,:)'); 
% % % % 
% % % %     fclose(fileID);
% % % % 
% % % % 
% % % %     % Rrs_AWI_M99_MH 
% % % %     
% % % %     File        = ['FRM4SOC_2018_', Team, '_Rrs_Mobley99.txt'];
% % % %     
% % % %     clear Data 
% % % %     
% % % %     Data(1,:)       = [0 lambda_OLCI]; 
% % % %     Data(2:36,1)    = Stations;
% % % %     Data(2:36,2:20) = R2; 
% % % %     
% % % %     fileID          = fopen(File, 'w+');
% % % % 
% % % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % % %     fprintf(fileID, '%3.0f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f\r\n', Data(2:end,:)'); 
% % % % 
% % % %     fclose(fileID);
% % % % 
% % % %     % rho_real_AWI 
% % % %     
% % % %     File        = ['FRM4SOC_2018_', Team, '_rho.txt'];
% % % %     
% % % %     clear Data 
% % % %     
% % % %     Data(1,:)       = [0 lambda_OLCI]; 
% % % %     Data(2:36,1)    = Stations;
% % % %     Data(2:36,2:20) = R3; 
% % % %     
% % % %     fileID          = fopen(File, 'w+');
% % % % 
% % % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % % %     fprintf(fileID, '%3.0f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f %10.7f\r\n', Data(2:end,:)'); 
% % % % 
% % % %     fclose(fileID);
% % % 
% % % 
% % % 
% % % end 


% % %     % CV_mean_EdHZG_MH 
% % %     
% % % %     File        = ['FRM4SOC_2018_CV_Rrs_same_EdHZG.txt'];
% % %     File        = ['FRM4SOC_2018_CV_Rrs_same_EdHZG_and_rhoM99.txt'];
% % %     
% % %     clear Data 
% % %     
% % %     Data(1,:)       = [0 lambda_OLCI]; 
% % %     Data(2:36,1)    = Stations;
% % %     Data(2:36,2:20) = CV_mean_EdHZG_MH; 
% % %     
% % %     fileID          = fopen(File, 'w+');
% % % 
% % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % %     fprintf(fileID, '%3.0f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\r\n', Data(2:end,:)'); 
% % % 
% % %     fclose(fileID);
% % % 
% % % 
% % %     % CV_mean_LskyHZG_MH 
% % %     
% % % %     File        = ['FRM4SOC_2018_CV_Rrs_same_LskyHZG.txt'];
% % %     File        = ['FRM4SOC_2018_CV_Rrs_same_LskyHZG_and_rhoM99.txt'];
% % %     
% % %     clear Data 
% % %     
% % %     Data(1,:)       = [0 lambda_OLCI]; 
% % %     Data(2:36,1)    = Stations;
% % %     Data(2:36,2:20) = CV_mean_LskyHZG_MH; 
% % %     
% % %     fileID          = fopen(File, 'w+');
% % % 
% % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % %     fprintf(fileID, '%3.0f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\r\n', Data(2:end,:)'); 
% % % 
% % %     fclose(fileID);
% % % 
% % % 
% % %     % CV_mean_LuHZG_MH 
% % %     
% % % %     File        = ['FRM4SOC_2018_CV_Rrs_same_LuHZG.txt'];
% % %     File        = ['FRM4SOC_2018_CV_Rrs_same_LuHZG_and_rhoM99.txt'];
% % %     
% % %     clear Data 
% % %     
% % %     Data(1,:)       = [0 lambda_OLCI]; 
% % %     Data(2:36,1)    = Stations;
% % %     Data(2:36,2:20) = CV_mean_LuHZG_MH; 
% % %     
% % %     fileID          = fopen(File, 'w+');
% % % 
% % %     fprintf(fileID, '%3.0f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n', Data(1,:)'); 
% % %     fprintf(fileID, '%3.0f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f\r\n', Data(2:end,:)'); 
% % % 
% % %     fclose(fileID);
% % % 


    
    
    
    

%%



% figure(1)
% clf
% 
% % plot(Wind, rho_R06, 's', 'MarkerFaceColor', [0.5 0.5 0.5], 'MarkerSize', 8)
% plot(Wind, rho_R06, '-', 'Color', [0.5 0.5 0.5])
% hold on
% plot(Wind, rho_real_RBINS, 'g.', 'MarkerSize', 8)
% plot(Wind, rho_real_CIMA, 'r.', 'MarkerSize', 8)
% plot(Wind, rho_real_TO, 'c.', 'MarkerSize', 8)
% plot(Wind, rho_real_UVIC, 'm.', 'MarkerSize', 8)
% plot(Wind, rho_real_PML, 'y.', 'MarkerSize', 8)
% plot(Wind, rho_real_AWI, 'k.', 'MarkerSize', 8)
% plot(Wind, rho_real_HZG, 'b.', 'MarkerSize', 8)
% 
%     grid
% 
%     axis([0 4.5 0 0.1]);
%     text(0.3, 0.085, ' \rho = (L_u - (E_d* R_r_s))/ L_s_k_y ', 'BackGroundColor', [1 1 1])
%     text(0.3, 0.01, ' \rho(R06) = 0.0256 + 0.00039* Wind + 0.000034* Wind^2 ' , 'BackGroundColor', [1 1 1], 'Color', [0.5 0.5 0.5])
% 
%     title('FRM4SOC - FICE - AAOT 2018  ');
%     xlabel('Wind speed   [ m s^-^1 ]');
%     ylabel('Surface reflectance factor  \rho  [ - ]')
% 
% % legend('R06', 'CIMA', 'RBINS', 'TO', 'UVIC', 'PML', 'AWI', 'HZG');
% 
% 
% %%
% 
% for p = 1: 35
% 
%     figure(2)
%     clf
% 
%     subplot(2,2,1);
% 
%     plot(lambda_OLCI, Rrs_CIMA(p,:), 'r+-')
%     hold on
%     plot(lambda_OLCI, Rrs_RBINS(p,:), 'g+-')
%     plot(lambda_OLCI, Rrs_TO(p,:), 'c+-')
%     plot(lambda_OLCI, Rrs_UVIC(p,:), 'm+-')
%     plot(lambda_OLCI, Rrs_PML(p,:), '+-', 'Color', [1 0.8 0])
%     plot(lambda_OLCI, Rrs_AWI(p,:), 'k+-')
%     plot(lambda_OLCI, Rrs_HZG(p,:), 'b+-')
% %     plot(lambda_OLCI, Rrs_mean(p,:), 'o-', 'Color', [0.5 0.5 0.5], 'LineWidth', 2)
%     plot(lambda_LOV, Rrs_LOV(p,:), '+--', 'Color', [0 0.5 0])
% 
%     grid
% 
%     axis([380 920 -0.0005 0.008]);
% 
%     legend('CIMA', 'RBINS', 'TO', 'UVIC', 'PML', 'AWI', 'HZG', 'LOV')
% 
%     title(['FRM4SOC - FICE - AAOT 2018  Station: ', num2str(Stations(p))]);
%     xlabel('\lambda  [ nm ]');
%     ylabel('R_r_s  [ sr^-^1 ]')
% 
% 
%     subplot(2,2,2);
% 
%     plot(lambda_OLCI, Rrs_CIMA_R06(p,:), 'r+-')
%     hold on
%     plot(lambda_OLCI, Rrs_RBINS_R06(p,:), 'g+-')
%     plot(lambda_OLCI, Rrs_TO_R06(p,:), 'c+-')
%     plot(lambda_OLCI, Rrs_UVIC_R06(p,:), 'm+-')
%     plot(lambda_OLCI, Rrs_PML_R06(p,:), '+-', 'Color', [1 0.8 0])
%     plot(lambda_OLCI, Rrs_AWI_R06(p,:), 'k+-')
%     plot(lambda_OLCI, Rrs_HZG_R06(p,:), 'b+-')
% %     plot(lambda_OLCI, Rrs_mean_R06(p,:), 'o-', 'Color', [0.5 0.5 0.5], 'LineWidth', 2)
%     grid
% 
%     axis([380 920 -0.0005 0.008]);
% 
%     % legend('CIMA', 'RBINS', 'TO', 'UVIC', 'PML', 'AWI', 'HZG', 'Mean')
% 
%     title(['With same \rho of ', num2str(rho_R06(p))]);
%     xlabel('\lambda  [ nm ]');
%     ylabel('R_r_s  [ sr^-^1 ]')
% 
%     
%     subplot(2,2,3);
% 
%     plot(lambda_OLCI2, Rrs_CIMA_1st(p,:), 'r+-')
%     hold on
%     plot(lambda_OLCI2, Rrs_RBINS_1st(p,:), 'g+-')
%     plot(lambda_OLCI2, Rrs_TO_1st(p,:), 'c+-')
%     plot(lambda_OLCI2, Rrs_UVIC_1st(p,:), 'm+-')
%     plot(lambda_OLCI2, Rrs_PML_1st(p,:), '+-', 'Color', [1 0.8 0])
%     plot(lambda_OLCI2, Rrs_AWI_1st(p,:), 'k+-')
%     plot(lambda_OLCI2, Rrs_HZG_1st(p,:), 'b+-')
%     plot(lambda_LOV2, Rrs_LOV_1st(p,:), '+--', 'Color', [0 0.5 0])
%     grid
%     
%     axis([380 920 -0.0001 0.0001]);
% 
%     xlabel('\lambda  [ nm ]');
%     ylabel('1st derivative of R_r_s  ')
%     
%     
%     subplot(2,2,4);
% 
% %         plot(BandRatio_CIMA(p,:), 'r+', 'Markersize', 20)
% %         hold on
% %         plot(BandRatio_RBINS(p,:), 'g+', 'Markersize', 20)
% %         plot(BandRatio_TO(p,:), 'c+', 'Markersize', 20)
% %         plot(BandRatio_UVIC(p,:), 'm+', 'Markersize', 20)
% %         plot(BandRatio_PML(p,:), '+', 'Markersize', 20, 'Color', [1 0.8 0])
% %         plot(BandRatio_AWI(p,:), 'k+', 'Markersize', 20)
% %         plot(BandRatio_HZG(p,:), 'b+', 'Markersize', 20)
% 
%         line([0.8 1.2], [BandRatio_CIMA(p,1) BandRatio_CIMA(p,1)], 'Color', [1 0 0], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_CIMA(p,2) BandRatio_CIMA(p,2)], 'Color', [1 0 0], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_CIMA(p,3) BandRatio_CIMA(p,3)], 'Color', [1 0 0], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_RBINS(p,1) BandRatio_RBINS(p,1)], 'Color', [0 1 0], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_RBINS(p,2) BandRatio_RBINS(p,2)], 'Color', [0 1 0], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_RBINS(p,3) BandRatio_RBINS(p,3)], 'Color', [0 1 0], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_TO(p,1) BandRatio_TO(p,1)], 'Color', [0 1 1], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_TO(p,2) BandRatio_TO(p,2)], 'Color', [0 1 1], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_TO(p,3) BandRatio_TO(p,3)], 'Color', [0 1 1], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_UVIC(p,1) BandRatio_UVIC(p,1)], 'Color', [1 0 1], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_UVIC(p,2) BandRatio_UVIC(p,2)], 'Color', [1 0 1], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_UVIC(p,3) BandRatio_UVIC(p,3)], 'Color', [1 0 1], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_PML(p,1) BandRatio_PML(p,1)], 'Color', [1 0.8 0], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_PML(p,2) BandRatio_PML(p,2)], 'Color', [1 0.8 0], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_PML(p,3) BandRatio_PML(p,3)], 'Color', [1 0.8 0], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_AWI(p,1) BandRatio_AWI(p,1)], 'Color', [0 0 0], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_AWI(p,2) BandRatio_AWI(p,2)], 'Color', [0 0 0], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_AWI(p,3) BandRatio_AWI(p,3)], 'Color', [0 0 0], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_HZG(p,1) BandRatio_HZG(p,1)], 'Color', [0 0 1], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_HZG(p,2) BandRatio_HZG(p,2)], 'Color', [0 0 1], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_HZG(p,3) BandRatio_HZG(p,3)], 'Color', [0 0 1], 'LineWidth', 2)
%         
%         line([0.8 1.2], [BandRatio_LOV(p,1) BandRatio_LOV(p,1)], 'LineStyle', '--', 'Color', [0 0.5 0], 'LineWidth', 2)
%         line([1.8 2.2], [BandRatio_LOV(p,2) BandRatio_LOV(p,2)], 'LineStyle', '--', 'Color', [0 0.5 0], 'LineWidth', 2)
%         line([2.8 3.2], [BandRatio_LOV(p,3) BandRatio_LOV(p,3)], 'LineStyle', '--', 'Color', [0 0.5 0], 'LineWidth', 2)
%         
%         
%         
%         
%         axis([0.5 3.5 0.8 1.3]) 
%         grid 
%         
%         set(gca, 'XTick', 1:3, 'XTickLabel', [{'R_r_s(443)/R_r_s(560)'}, {'R_r_s(490)/R_r_s(560)'}, {'R_r_s(510)/R_r_s(560)'}]);
%         
%         ylabel('Band ratio') 
%         
% %     plot(lambda_OLCI2, Rrs_CIMA_R06_1st(p,:), 'r+-')
% %     hold on
% %     plot(lambda_OLCI2, Rrs_RBINS_R06_1st(p,:), 'g+-')
% %     plot(lambda_OLCI2, Rrs_TO_R06_1st(p,:), 'c+-')
% %     plot(lambda_OLCI2, Rrs_UVIC_R06_1st(p,:), 'm+-')
% %     plot(lambda_OLCI2, Rrs_PML_R06_1st(p,:), '+-', 'Color', [1 0.8 0])
% %     plot(lambda_OLCI2, Rrs_AWI_R06_1st(p,:), 'k+-')
% %     plot(lambda_OLCI2, Rrs_HZG_R06_1st(p,:), 'b+-')
% %     grid
% %     
% %     axis([380 920 -0.0001 0.0001]);
% % 
% %     xlabel('\lambda  [ nm ]');
% %     ylabel('1st derivative of R_r_s  ')
%     
% 
%     
% % %     filename    = ['FRM4SOC_Rrs_', num2str(Stations(p)), '.png'];
% % %     saveas(gcf, filename, 'png');
% 
% %     filename    = ['FRM4SOC_Rrs_', num2str(Stations(p)), '.tif'];
% %     print(gcf, '-dtiff', filename, '-r100');
% 
% 
% end 


%%

% JET = colormap(gray(19));

figure(4)
clf

set(gcf, 'Position', [550 100 500 500]); 
subplot('Position', [0.12 0.56 0.38 0.38])

    for i = 1: 10
        scatter(CV_mean(Viewing_select==90,i), CV_mean_M99_MH(Viewing_select==90,i), 20, OLCI_VIS_Colors(i,2:end)/255, 'filled')
        hold on
        scatter(CV_mean(Viewing_select==135,i), CV_mean_M99_MH(Viewing_select==135,i), 20, OLCI_VIS_Colors(i,2:end)/255, 's')
    end
    grid
    axis([0 20 0 20])
    line([0 20], [0 20], 'Color', [0 0 0])
    set(gca, 'XTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(2, 18, 'Same \rho  [Mobley, 1999]', 'BackGroundColor', [1 1 1], 'FontSize', 8)
%     ylabel('CV of R_r_s')
    box on
    
    
subplot('Position', [0.54 0.56 0.38 0.38])

    for i = 1: 10
        scatter(CV_mean(Viewing_select==90,i), CV_mean_EdHZG_MH(Viewing_select==90,i), 20, OLCI_VIS_Colors(i,2:end)/255, 'filled')
        hold on
        scatter(CV_mean(Viewing_select==135,i), CV_mean_EdHZG_MH(Viewing_select==135,i), 20, OLCI_VIS_Colors(i,2:end)/255, 's')
    end
    grid
    axis([0 20 0 20])
    line([0 20], [0 20], 'Color', [0 0 0])
    set(gca, 'XTickLabel', []); 
    set(gca, 'YTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(2, 18, 'Same E_d', 'BackGroundColor', [1 1 1], 'FontSize', 8)
    box on

subplot('Position', [0.12 0.14 0.38 0.38])

    for i = 1: 10
        scatter(CV_mean(Viewing_select==90,i), CV_mean_LuHZG_MH(Viewing_select==90,i), 20, OLCI_VIS_Colors(i,2:end)/255, 'filled')
        hold on
        scatter(CV_mean(Viewing_select==135,i), CV_mean_LuHZG_MH(Viewing_select==135,i), 20, OLCI_VIS_Colors(i,2:end)/255, 's')
    end
    grid
    axis([0 20 0 20])
    line([0 20], [0 20], 'Color', [0 0 0])
%     set(gca, 'XTickLabel', []); 
%     set(gca, 'YTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(2, 18, 'Same L_t', 'BackGroundColor', [1 1 1], 'FontSize', 8)
    box on
    text(-4, 10, 'CV of R_r_s with same \rho, E_d, L_t or L_s_k_y', 'Rotation', 90)
    text(17, -5, 'CV of original R_r_s')


subplot('Position', [0.54 0.14 0.38 0.38])

    for i = 1: 10
        scatter(CV_mean(Viewing_select==90,i), CV_mean_LskyHZG_MH(Viewing_select==90,i), 20, OLCI_VIS_Colors(i,2:end)/255, 'filled')
        hold on
        scatter(CV_mean(Viewing_select==135,i), CV_mean_LskyHZG_MH(Viewing_select==135,i), 20, OLCI_VIS_Colors(i,2:end)/255, 's')
    end
    grid
    axis([0 20 0 20])
    line([0 20], [0 20], 'Color', [0 0 0])
    set(gca, 'YTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(2, 18, 'Same L_s_k_y', 'BackGroundColor', [1 1 1], 'FontSize', 8)
    box on
%     xlabel('CV of R_r_s')


    
% %     filename    = ['FRM4SOC_CV_Rrs_allinone.tif'];
%     filename    = ['FRM4SOC_CV_Rrs_allinone_with_same_rhoM99.tif'];
%     print(gcf, '-dtiff', filename, '-r100');
    
    
    
%% 

% return 
% 
% 
% 
% %%
% 
% figure(400)
% clf
% 
% for i = 1: 10
%     scatter(CV_mean(:,i), CV_mean_EdHZG_MH(:,i), 10, JET(i,:), 'filled')
%     hold on
% end
% grid
% axis([0 20 0 20])
% line([0 20], [0 20], 'Color', [1 0 0])
% 
% title('Coefficient of Variations of R_r_s');
% xlabel('CV from original R_r_s  [ % ]');
% ylabel('CV from R_r_s with same E_d')
% box on 
% text(10, 90, 'CV = 100* std(Rrs)/ mean(Rrs)', 'BackGroundColor', [1 1 1])
% 
% % legend('400', '412', '442', '490', '510', '560', '620', '665', '674', '681', '709', '754', '761', '764', '767', '779', '865', '885', '900', 'Location', 'EastOutside')
% legend('400', '412', '442', '490', '510', '560', '620', '665', '674', '681', '709', '754', '761', '764', '767', '779', '865', '885', '900', 'Location', 'EastOutside')
% 
% % %     filename    = ['FRM4SOC_CV_Rrs_same_Ed_all_stations.tif'];
% %     filename    = ['FRM4SOC_CV_Rrs_same_Ed_and_rhoM99_all_stations.tif'];
% %     print(gcf, '-dtiff', filename, '-r100');
% 
% 
% figure(401)
% clf
% 
% for i = 1: 10
%     scatter(CV_mean(:,i), CV_mean_LuHZG_MH(:,i), 10, JET(i,:), 'filled')
%     hold on
% end
% grid
% axis([0 20 0 20])
% line([0 20], [0 20], 'Color', [1 0 0])
% 
% title('Coefficient of Variations of R_r_s');
% xlabel('CV from original R_r_s  [ % ]');
% ylabel('CV from R_r_s with same L_u')
% box on 
% text(10, 90, 'CV = 100* std(Rrs)/ mean(Rrs)', 'BackGroundColor', [1 1 1])
% 
% % legend('400', '412', '442', '490', '510', '560', '620', '665', '674', '681', '709', '754', '761', '764', '767', '779', '865', '885', '900', 'Location', 'EastOutside')
% legend('400', '412', '442', '490', '510', '560', '620', '665', '674', '681', '709', '754', '761', '764', '767', '779', '865', '885', '900', 'Location', 'EastOutside')
% 
% 
% % %     filename    = ['FRM4SOC_CV_Rrs_same_Lu_all_stations.tif'];
% %     filename    = ['FRM4SOC_CV_Rrs_same_Lu_and_rhoM99_all_stations.tif'];
% %     print(gcf, '-dtiff', filename, '-r100');
%     
% 
% figure(402)
% clf
% 
% for i = 1: 10
%     scatter(CV_mean(:,i), CV_mean_LskyHZG_MH(:,i), 10, JET(i,:), 'filled')
%     hold on
% end
% grid
% axis([0 20 0 20])
% line([0 20], [0 20], 'Color', [1 0 0])
% 
% title('Coefficient of Variations of R_r_s');
% xlabel('CV from original R_r_s  [ % ]');
% ylabel('CV from R_r_s with same L_s_k_y')
% box on 
% text(10, 90, 'CV = 100* std(Rrs)/ mean(Rrs)', 'BackGroundColor', [1 1 1])
% 
% % legend('400', '412', '442', '490', '510', '560', '620', '665', '674', '681', '709', '754', '761', '764', '767', '779', '865', '885', '900', 'Location', 'EastOutside')
% legend('400', '412', '442', '490', '510', '560', '620', '665', '674', '681', '709', '754', '761', '764', '767', '779', '865', '885', '900', 'Location', 'EastOutside')
% 
% % %     filename    = ['FRM4SOC_CV_Rrs_same_Lsky_all_stations.tif'];
% %     filename    = ['FRM4SOC_CV_Rrs_same_Lsky_and_rhoM99_all_stations.tif'];
% %     print(gcf, '-dtiff', filename, '-r100');




%% Reduction in CV 


figure(5000)
clf

set(gcf, 'Position', [550 100 500 500]); 
subplot('Position', [0.12 0.56 0.38 0.38])

    plot(lambda_OLCI(1:10), Reduction_in_CV(Stations_select_Nr, 1:10), 'b+')
    hold on
    plot(lambda_OLCI(1:10), mean(Reduction_in_CV(Stations_select_Nr, 1:10), 1), 'r')
    grid
    axis([380 720 -100 100])

%     xlabel('\lambda   [ nm ]');
%     ylabel('Change of CV of R_r_s  [ % ]')
    set(gca, 'XTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(410, 85, 'Same \rho  [Mobley, 1999]', 'BackGroundColor', [1 1 1], 'FontSize', 8)
%     ylabel('CV of R_r_s')
    box on
    
    
subplot('Position', [0.54 0.56 0.38 0.38])

    plot(lambda_OLCI(1:10), Reduction_in_CV_EdHZG(Stations_select_Nr, 1:10), 'b+')
    hold on
    plot(lambda_OLCI(1:10), mean(Reduction_in_CV_EdHZG(Stations_select_Nr, 1:10), 1), 'r')
    grid
    axis([380 720 -100 100])

    set(gca, 'XTickLabel', []); 
    set(gca, 'YTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(410, 85, 'Same E_d', 'BackGroundColor', [1 1 1], 'FontSize', 8)
    box on
    

subplot('Position', [0.12 0.14 0.38 0.38])

    plot(lambda_OLCI(1:10), Reduction_in_CV_LuHZG(Stations_select_Nr, 1:10), 'b+')
    hold on
    plot(lambda_OLCI(1:10), mean(Reduction_in_CV_LuHZG(Stations_select_Nr, 1:10), 1), 'r')
    grid
    axis([380 720 -100 100])

    set(gca, 'FontSize', 8); 
    text(410, 85, 'Same L_t', 'BackGroundColor', [1 1 1], 'FontSize', 8)
    box on
    text(300, 70, 'Change of CV', 'Rotation', 90)
    text(620, -140, 'Wavelength  \lambda  [ nm ]')

    
subplot('Position', [0.54 0.14 0.38 0.38])

    plot(lambda_OLCI(1:10), Reduction_in_CV_LskyHZG(Stations_select_Nr, 1:10), 'b+')
    hold on
    plot(lambda_OLCI(1:10), mean(Reduction_in_CV_LskyHZG(Stations_select_Nr, 1:10), 1), 'r')
    grid
    axis([380 720 -100 100])

    set(gca, 'YTickLabel', []); 
    set(gca, 'FontSize', 8); 
    text(410, 85, 'Same L_s_k_y', 'BackGroundColor', [1 1 1], 'FontSize', 8)
    box on
%     xlabel('CV of R_r_s')


% %     filename    = ['FRM4SOC_Reduction_of_CV_Rrs_allinone.tif'];
%     filename    = ['FRM4SOC_Reduction_of_CV_Rrs_allinone_with_same_rhoM99.tif'];
%     print(gcf, '-dtiff', filename, '-r100');


% return 
% 
% 
% figure(45)
% clf
% 
% plot(lambda_OLCI, Reduction_in_CV, 'b+')
% hold on
% plot(lambda_OLCI, mean(Reduction_in_CV([1:13, 16:end], :), 1), 'r')
% grid
% axis([380 920 -100 100])
% title('Change in variability using same \rho')
% 
% xlabel('\lambda   [ nm ]');
% ylabel('Change of CV of R_r_s  [ % ]')
% 
% 
% %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_rho.tif';
% %     print(gcf, '-dtiff', filename, '-r100');
% 
%     
% 
% figure(451)
% clf
% 
% plot(lambda_OLCI, Reduction_in_CV_EdHZG, 'b+')
% hold on
% plot(lambda_OLCI, mean(Reduction_in_CV_EdHZG([1:13, 16:end], :), 1), 'r')
% grid
% axis([380 920 -100 100])
% title('Change in variability using same E_d')
% % title('Change in variability using same E_d and \rho')
% 
% 
% xlabel('\lambda   [ nm ]');
% ylabel('Change of CV of R_r_s  [ % ]')
% 
% 
% % %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_Ed.tif';
% %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_Ed_and_rhoM99.tif';
% %     print(gcf, '-dtiff', filename, '-r100');
% 
% 
% figure(452)
% clf
% 
% plot(lambda_OLCI, Reduction_in_CV_LskyHZG, 'b+')
% hold on
% plot(lambda_OLCI, mean(Reduction_in_CV_LskyHZG([1:13, 16:end], :), 1), 'r')
% grid
% axis([380 920 -100 100])
% title('Change in variability using same L_s_k_y')
% % title('Change in variability using same L_s_k_y and \rho')
% 
% xlabel('\lambda   [ nm ]');
% ylabel('Change of CV of R_r_s  [ % ]')
% 
% 
% % %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_Lsky.tif';
% %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_Lsky_and_rhoM99.tif';
% %     print(gcf, '-dtiff', filename, '-r100');
% 
% 
% figure(453)
% clf
% 
% plot(lambda_OLCI, Reduction_in_CV_LuHZG, 'b+')
% hold on
% plot(lambda_OLCI, mean(Reduction_in_CV_LuHZG([1:13, 16:end], :), 1), 'r')
% grid
% axis([380 920 -100 100])
% title('Change in variability using same L_u')
% % title('Change in variability using same L_u and \rho')
% 
% xlabel('\lambda   [ nm ]');
% ylabel('Change of CV of R_r_s  [ % ]')
% 
% % %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_Lu.tif';
% %     filename    = 'FRM4SOC_Change_in_CV_Rrs_same_Lu_and_rhoM99.tif';
% %     print(gcf, '-dtiff', filename, '-r100');
% 
% 





%%

% figure(44)
% clf
% 
% subplot(2,2,1)
%     imagesc(CV_mean, [-100 100])
%     title('CV mean')
% subplot(2,2,2)
%     imagesc(CV_mean_all, [-100 100])
%     title('CV mean incl LOV')
% subplot(2,2,3)
%     imagesc(CV_mean_R06, [-100 100])
%     title('CV R06')
% subplot(2,2,4)
%     imagesc(CV_mean_M99_MH, [-100 100])
%     title('CV M99')
    



%%

% figure(5)
% clf
% 
%     plot(lambda_OLCI2, Rrs_CIMA_1st, 'r+-')
%     hold on
%     plot(lambda_OLCI2, Rrs_RBINS_1st, 'g+-')
%     plot(lambda_OLCI2, Rrs_TO_1st, 'c+-')
%     plot(lambda_OLCI2, Rrs_UVIC_1st, 'm+-')
%     plot(lambda_OLCI2, Rrs_PML_1st, 'y+-')
%     plot(lambda_OLCI2, Rrs_AWI_1st, 'k+-')
%     plot(lambda_OLCI2, Rrs_HZG_1st, 'b+-')
%     grid
% %     axis([0 80 0 80])
% %     line([0 80], [0 80], 'Color', [1 0 0])
% 
% %     title('Comparison of the Coefficient of Variations using constant \rho');
% %     xlabel('CV from original R_r_s  [ % ]');
% %     ylabel('CV from R_r_s with same \rho  [ % ]')



%%

% figure(10)
% clf
% 
% plot(Stations, round(rho_R06, 4), '+')
% hold on
% plot(Stations, rho_R06_MH, 'ro')
% plot(Stations, rho_M99_MH, 'gs')
% grid




%%
% 
% for p = 4%1: 35
% 
%     figure(20)
%     clf
% 
%     subplot(2,2,1);
% 
%     plot(lambda_OLCI, Rrs_CIMA(p,:), 'r+-')
%     hold on
%     plot(lambda_OLCI, Rrs_RBINS(p,:), 'g+-')
%     plot(lambda_OLCI, Rrs_TO(p,:), 'c+-')
%     plot(lambda_OLCI, Rrs_UVIC(p,:), 'm+-')
%     plot(lambda_OLCI, Rrs_PML(p,:), '+-', 'Color', [1 0.8 0])
%     plot(lambda_OLCI, Rrs_AWI(p,:), 'k+-')
%     plot(lambda_OLCI, Rrs_HZG(p,:), 'b+-')
% %     plot(lambda_OLCI, Rrs_mean(p,:), 'o-', 'Color', [0.5 0.5 0.5], 'LineWidth', 2)
%     plot(lambda_LOV, Rrs_LOV(p,:), '+--', 'Color', [0 0.5 0])
% 
%     grid
% 
%     axis([380 920 -0.0005 0.008]);
% 
%     legend('CIMA', 'RBINS', 'TO', 'UVIC', 'PML', 'AWI', 'HZG', 'LOV')
% 
%     title(['FRM4SOC - FICE - AAOT 2018   Station: ', num2str(Stations(p))]);
%     xlabel('\lambda  [ nm ]');
%     ylabel('R_r_s  [ sr^-^1 ]')
% 
% 
%     subplot(2,2,2);
%    
%     plot(lambda_OLCI, Rrs_CIMA_M99_MH(p,:), 'r+-')
%     hold on
%     plot(lambda_OLCI, Rrs_RBINS_M99_MH(p,:), 'g+-')
%     plot(lambda_OLCI, Rrs_TO_M99_MH(p,:), 'c+-')
%     plot(lambda_OLCI, Rrs_UVIC_M99_MH(p,:), 'm+-')
%     plot(lambda_OLCI, Rrs_PML_M99_MH(p,:), '+-', 'Color', [1 0.8 0])
%     plot(lambda_OLCI, Rrs_AWI_M99_MH(p,:), 'k+-')
%     plot(lambda_OLCI, Rrs_HZG_M99_MH(p,:), 'b+-')
%     
% %     plot(lambda_OLCI, Rrs_mean_M99_MH(p,:), 'o-', 'Color', [0.5 0.5 0.5], 'LineWidth', 2)
%     grid
% 
%     axis([380 920 -0.0005 0.008]);
% 
%     % legend('CIMA', 'RBINS', 'TO', 'UVIC', 'PML', 'AWI', 'HZG', 'Mean')
% 
%     title(['With same \rho of ', num2str(rho_M99_MH(p), 4), '  [Mobley, 1999]']);
%     xlabel('\lambda  [ nm ]');
%     ylabel('R_r_s  [ sr^-^1 ]')
% 
%     
%     subplot(2,2,3);
% 
%     plot(lambda_OLCI2, Rrs_CIMA_1st(p,:), 'r+-')
%     hold on
%     plot(lambda_OLCI2, Rrs_RBINS_1st(p,:), 'g+-')
%     plot(lambda_OLCI2, Rrs_TO_1st(p,:), 'c+-')
%     plot(lambda_OLCI2, Rrs_UVIC_1st(p,:), 'm+-')
%     plot(lambda_OLCI2, Rrs_PML_1st(p,:), '+-', 'Color', [1 0.8 0])
%     plot(lambda_OLCI2, Rrs_AWI_1st(p,:), 'k+-')
%     plot(lambda_OLCI2, Rrs_HZG_1st(p,:), 'b+-')
%     plot(lambda_LOV2, Rrs_LOV_1st(p,:), '+--', 'Color', [0 0.5 0])
%     grid
%     
%     axis([380 920 -0.0001 0.0001]);
% 
%     xlabel('\lambda  [ nm ]');
%     ylabel('1st derivative of R_r_s  ')
%     title('Spectral shape of R_r_s');
%     
%     
%     subplot(2,2,4);
% 
%         line([380 920], [rho_M99_MH(p) rho_M99_MH(p)], 'LineStyle', '--', 'Color', [0.8 0.8 0.8], 'LineWidth', 2) 
%         hold on
% 
%         plot(lambda_OLCI, rho_real_CIMA(p,:), 'r+-')
%         plot(lambda_OLCI, rho_real_RBINS(p,:), 'g+-')
%         plot(lambda_OLCI, rho_real_TO(p,:), 'c+-')
%         plot(lambda_OLCI, rho_real_UVIC(p,:), 'm+-')
%         plot(lambda_OLCI, rho_real_PML(p,:), '+-', 'Color', [1 0.8 0])
%         plot(lambda_OLCI, rho_real_AWI(p,:), 'k+-')
%         plot(lambda_OLCI, rho_real_HZG(p,:), 'b+-')
%     %     plot(lambda_OLCI, rho_real_mean_M99_MH(p,:), 'o-', 'Color', [0.5 0.5 0.5], 'LineWidth', 2)
%     
%     
%         axis([380 920 0.02 0.06]) 
%         grid 
%         box on
% %         set(gca, 'XTick', 1:3, 'XTickLabel', [{'R_r_s(443)/R_r_s(560)'}, {'R_r_s(490)/R_r_s(560)'}, {'R_r_s(510)/R_r_s(560)'}]);
%         
%         xlabel('\lambda  [ nm ]');
%         ylabel('\rho  [ - ]')
%     
%     text(410, 0.055, ' \rho = (L_u - (E_d* R_r_s))/ L_s_k_y ', 'BackGroundColor', [1 1 1])
%     text(750, 0.055, ' Mobley [1999] ', 'Color', [0.8 0.8 0.8], 'BackGroundColor', [1 1 1])
% 
%     title('Effective surface reflectance factor');
%     
% % %     filename    = ['FRM4SOC_Rrs_', num2str(Stations(p)), '.png'];
% % %     saveas(gcf, filename, 'png');
% 
% %     filename    = ['FRM4SOC_Rrs_1stdev_rho_', num2str(Stations(p)), '.tif'];
% %     print(gcf, '-dtiff', filename, '-r100');
% 
% 
% end 
% 




%%

% figure(888)
% clf
% 
% plot(lambda_OLCI, Ed_HZG(7,:))
% hold on
% plot(lambda_OLCI, Ed_TO(7,:))
% plot(lambda_OLCI, Ed_CIMA(7,:))
% plot(lambda_OLCI, Ed_RBINS(7,:))
% plot(lambda_OLCI, Ed_UVIC(7,:))
% plot(lambda_OLCI, Ed_PML(7,:))
% plot(lambda_OLCI, Ed_AWI(7,:))
% grid
% legend('HZG', 'TO', 'CIMA', 'RBINS', 'UVIC', 'PML', 'AWI')
% xlabel('wavelength')
% ylabel('E_d')
% title('E_d at station 7')
% 
% figure(889)
% clf
% 
% plot(lambda_OLCI, Lsky_HZG(7,:))
% hold on
% plot(lambda_OLCI, Lsky_TO(7,:))
% plot(lambda_OLCI, Lsky_CIMA(7,:))
% plot(lambda_OLCI, Lsky_RBINS(7,:))
% plot(lambda_OLCI, Lsky_UVIC(7,:))
% plot(lambda_OLCI, Lsky_PML(7,:))
% plot(lambda_OLCI, Lsky_AWI(7,:))
% grid
% legend('HZG', 'TO', 'CIMA', 'RBINS', 'UVIC', 'PML', 'AWI')
% xlabel('wavelength')
% ylabel('L_s_k_y')
% title('L_s_k_y at station 7')
% 
% 
% figure(887)
% clf
% 
% plot(lambda_OLCI, Lu_HZG(7,:))
% hold on
% plot(lambda_OLCI, Lu_TO(7,:))
% plot(lambda_OLCI, Lu_CIMA(7,:))
% plot(lambda_OLCI, Lu_RBINS(7,:))
% plot(lambda_OLCI, Lu_UVIC(7,:))
% plot(lambda_OLCI, Lu_PML(7,:))
% plot(lambda_OLCI, Lu_AWI(7,:))
% grid
% legend('HZG', 'TO', 'CIMA', 'RBINS', 'UVIC', 'PML', 'AWI')
% xlabel('wavelength')
% ylabel('L_u')
% title('L_u at station 7')







save -v6 data4grg.mat * 






