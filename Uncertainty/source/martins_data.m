clear all
pkg load nan

load ../data/data4grg.mat

clear CV_mean_anom Rrs_mean_anom Rrs_std_anom

% Rrs_anom = nan(size(Rrs_HZG),7);

Rrs_all = [Rrs_HZG; Rrs_CIMA; Rrs_RBINS; Rrs_TO; Rrs_UVIC; Rrs_PML; Rrs_AWI];
Ed_all = [Ed_HZG; Ed_CIMA; Ed_RBINS; Ed_TO; Ed_UVIC; Ed_PML; Ed_AWI];
Lu_all = [Lu_HZG; Lu_CIMA; Lu_RBINS; Lu_TO; Lu_UVIC; Lu_PML; Lu_AWI];
Lsky_all = [Lsky_HZG; Lsky_CIMA; Lsky_RBINS; Lsky_TO; Lsky_UVIC; Lsky_PML; Lsky_AWI];
rho_real_all = [rho_real_HZG; rho_real_CIMA; rho_real_RBINS; rho_real_TO; rho_real_UVIC; rho_real_PML; rho_real_AWI];


Rrs_anom = nan*Rrs_all;
Ed_anom = nan*Rrs_all;
Lu_anom = nan*Rrs_all;
Lsky_anom = nan*Rrs_all;
rho_real_anom = nan*Rrs_all;



# flag record 36, 514 because they are outliers
Rrs_all(36,:) = nan*Rrs_all(36,:);
Ed_all(36,:) = nan*Rrs_all(36,:);
Lu_all(36,:) = nan*Rrs_all(36,:);
Lsky_all(36,:) = nan*Rrs_all(36,:);
rho_real_all(36,:) = nan*Rrs_all(36,:);

Rrs_all(154,:) = nan*Rrs_all(154,:);
Ed_all(154,:) = nan*Rrs_all(154,:);
Lu_all(154,:) = nan*Rrs_all(154,:);
Lsky_all(154,:) = nan*Rrs_all(154,:);
rho_real_all(154,:) = nan*Rrs_all(154,:);


# remove mean from each station
meanRrs = nan(size(Rrs_PML));
meanEd = nan(size(Rrs_PML));
meanLu = nan(size(Rrs_PML));
meanLsky = nan(size(Rrs_PML));
meanRho = nan(size(Rrs_PML));

for ist = 1:35
	
	meanRrs(ist,:) = median(Rrs_all(ist:35:end, :));
	meanEd(ist,:) = median(Ed_all(ist:35:end, :));
	meanLu(ist,:) = median(Lu_all(ist:35:end, :));
	meanLsky(ist,:) = median(Lsky_all(ist:35:end, :));
	meanRho(ist,:) = median(rho_real_all(ist:35:end, :));
	
	Rrs_anom(ist:35:end, :) = Rrs_all(ist:35:end, :) - meanRrs(ist,:);
	Ed_anom(ist:35:end, :) = Ed_all(ist:35:end, :) - meanEd(ist,:);
	Lu_anom(ist:35:end, :) = Lu_all(ist:35:end, :) - meanLu(ist,:);
	Lsky_anom(ist:35:end, :) = Lsky_all(ist:35:end, :) - meanLsky(ist,:);
	rho_real_anom(ist:35:end, :) = rho_real_all(ist:35:end, :) - meanRho(ist,:);
	
	% [ist:35:length(Rrs_anom)](end)
	
	
	% figure(1)
% 		hist(Rrs_anom(ist:35:end, [1 5 10]))
%
% 	keyboard
	
endfor

% keyboard

#		wv, u2_Rrs, u2_Lsky, u2_Lu,	u2_Ed, u2_rho, dRrs2dLsky, dRrs2dLu, dRrs2dEd, dRrs2drho, r_Lsky_Lu, r_Lsky_Ed, r_Lu_Ed

b.u2_rrs = prcrng(Rrs_anom).^2;
u2_Ed = prcrng(Ed_anom).^2;
u2_Lu = prcrng(Lu_anom).^2;
u2_Lsky = prcrng(Lsky_anom).^2;
u2_rho = prcrng(rho_real_anom).^2;


% Rrs = (Lu - rho*Lsky)/Ed

dRrs2dLsky 	= median(-rho_real_all./Ed_all);
dRrs2dLu	= median(1./Ed_all);
dRrs2dEd 	= median(-Rrs_all./Ed_all);
dRrs2drho	= median(-Lsky_all./Ed_all);
 	
r_Lsky_Lu	= diag(corrcoef(Lu_anom, Lsky_anom, 'Pearson' ))';
r_Lsky_Ed	= diag(corrcoef(Ed_anom, Lsky_anom, 'Pearson' ))'; 
r_Lu_Ed		= diag(corrcoef(Lu_anom, Ed_anom, 'Pearson' ))'; 

r_Lu_rho	= diag(corrcoef(Lu_anom, rho_real_anom, 'Pearson' ))'; 
r_Lsky_rho	= diag(corrcoef(Lsky_anom, rho_real_anom, 'Pearson' ))'; 
r_Ed_rho	= diag(corrcoef(Ed_anom, rho_real_anom, 'Pearson' ))'; 


b.u2rrs_Lsky	=	u2_Lsky .* dRrs2dLsky.^2;
b.u2rrs_Lu		=	u2_Lu 	.* dRrs2dLu.^2;
b.u2rrs_Ed		=	u2_Ed 	.* dRrs2dEd.^2;
b.u2rrs_rho		=	u2_rho 	.* dRrs2drho.^2;

b.corr_Lsky_Ed	=	sqrt(u2_Lsky)	.* sqrt(u2_Ed)	.* dRrs2dLsky	.* dRrs2dEd		.* r_Lsky_Ed;
b.corr_Lsky_Lu	=	sqrt(u2_Lsky)	.* sqrt(u2_Lu)	.* dRrs2dLsky	.* dRrs2dLu		.* r_Lsky_Lu;
b.corr_Lsky_rho	=	sqrt(u2_Lsky)	.* sqrt(u2_rho)	.* dRrs2dLsky	.* dRrs2drho	.* r_Lsky_rho;

b.corr_Lu_Ed	=	sqrt(u2_Lu)		.* sqrt(u2_Ed)	.* dRrs2dLu		.* dRrs2dEd		.* r_Lu_Ed;
b.corr_Lu_rho	=	sqrt(u2_Lu)		.* sqrt(u2_rho)	.* dRrs2dLu		.* dRrs2drho	.* r_Lu_rho;

b.corr_Ed_rho	=   sqrt(u2_Ed)		.* sqrt(u2_rho)	.* dRrs2dEd		.* dRrs2drho	.* r_Ed_rho;


b.wv = lambda_OLCI;
% f = fieldnames(b);
% icorr = strmatch('corr_', f);
% iu2 = strmatch('u2rrs_', f);
%
% b.corr_anom = zeros(size(b.wv));
% for ic =1:length(icorr)
% 	f{icorr(ic)}
% 	b.corr_anom = b.(f{icorr(ic)}) + b.corr_anom;
% endfor

b.corr_anom = 2 * (b.corr_Lsky_Lu + b.corr_Lsky_Ed + b.corr_Lsky_rho + b.corr_Lu_Ed + b.corr_Lu_rho + b.corr_Ed_rho);

figure(2)
clf
hold on
	plot(b.wv, b.corr_Lsky_Lu, 		';L_{sky}, L_{t};',		lw, 2)
	plot(b.wv, b.corr_Lsky_Ed, 		';L_{sky}, E_{d};',		lw, 2)
	plot(b.wv, b.corr_Lsky_rho, 	';L_{sky}, \rho;',		lw, 2)
	plot(b.wv, b.corr_Lu_Ed, 		';L_{u}, E_{d};',		lw, 2)
	plot(b.wv, b.corr_Lu_rho, 		';L_{u}, \rho;',		lw, 2)
	plot(b.wv, b.corr_Ed_rho, 		';E_{d}, \rho;',		lw, 2)
	set(gca, fs, 20)
	box('on')
	% grid on
	xlim([400 700])

	xlabel("wavelength [nm]", fs, 20, fw, 'bold')
	
% b.u2_anom = zeros(size(b.wv));
% for ic =1:length(iu2)
% 	f{iu2(ic)}
% 	b.u2_anom = b.(f{iu2(ic)}) + b.u2_anom;
% endfor

b.u2_anom = b.u2rrs_Lsky + b.u2rrs_Lu + b.u2rrs_Ed + b.u2rrs_rho;

figure(1)
clf
hold on
	plot(b.wv, b.u2rrs_Lu./(b.u2_anom + b.corr_anom), 				lw, 2)
	plot(b.wv, b.u2rrs_Lsky./(b.u2_anom + b.corr_anom), 			lw, 2)
	plot(b.wv, b.u2rrs_Ed./(b.u2_anom + b.corr_anom), 			 	lw, 2)
	plot(b.wv, b.u2rrs_rho./(b.u2_anom + b.corr_anom), 				lw, 2)
	plot(b.wv, b.corr_anom./(b.u2_anom + b.corr_anom), 				lw, 2)
	

# from these plots below we can see that the estiamted variance including the correlation terms is a better representation of the measured variance in Rrs (e.g., in the first channel)
	% plot(b.wv, b.u2_anom, 					';Rrs_{est};', lw, 4)
	% plot(b.wv, b.u2_anom + b.corr_anom,	';Rrs_{est} with correls;', lw, 4)
	% plot(b.wv, b.u2_rrs, 					';Rrs_{meas};', lw, 4)

	set(gca, fs, 20)
	box('on')
	grid on
	xlim([400 700])
	ylim([-1.5 1.5])

	xlabel("wavelength [nm]", fs, 20, fw, 'bold')
	title("Fraction of Rrs variance due to each input", fs, 20, fw, 'bold')
	

	hl = legend({'Lu', 'L{sky}', 'Ed', '\rho', 'corr'}, "location", "northeast");
	set(hl, fs, 20, 'box', 'off', fw, 'bold')
	set(hl, 'pos', [0.71735  0.74976  0.075203  0.15616])
	
	print("-dpng", "contributions_2_rrs_variance_corr.png")




out = [b.wv; [b.u2rrs_Lu; b.u2rrs_Lsky; b.u2rrs_Ed; b.u2rrs_rho; b.corr_anom]./(b.u2_anom + b.corr_anom)];
save -ascii wv_Lu_Lsky_Ed_rho_corr.dat out


figure(3)
clf
hold on
	plot(b.wv, r_Lsky_Lu, 			';Lsky-Lu;', lw, 1)
	plot(b.wv, r_Lsky_Ed, 			';Lsky-Ed;', lw, 1)
	plot(b.wv, r_Lsky_rho, 			';Lsky-rho;', lw, 1)
	plot(b.wv, r_Lu_Ed, 			';Lu-Ed;', lw, 1)
	plot(b.wv, r_Lu_rho,			';Lu-rho;', lw, 1)
	plot(b.wv, r_Ed_rho, 			';Ed-rho;', lw, 1)
	plot(b.wv, r_anom,				';sum corr;', lw, 4)

	set(gca, fs, 20)






%
%
% figure(2)
% clf
% for iwv = 1:10
% 	% innan = find(~isnan(Lu_anom(:,iwv)));
% 	d = reshape(rho_real_anom(:,iwv), [35, 7]');
% 	subplot(121),
% 		plot(d, 'o', mfc, 'auto'),
% 		set(gca, 'ydir', 'reverse'),
% 		xlims = ylim();
% 	subplot(122)
% 		[counts, bins] = hist(d(:), 20);
% 		barh(bins, counts)
% 		set(gca, 'ydir', 'reverse')
% 		ylim(xlims)
% 		title(num2str(b.wv(iwv)))
% 		keyboard
% endfor
%
%
%
