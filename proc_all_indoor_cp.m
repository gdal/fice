clear all
close all

pkg load io

main_dir = "/fast_scratch/TO/Tartu/";
# main_dir = "~/Dropbox/Project/FRM4SOC/TO/Tartu/";


# these are the different input paraemters for all indoor experiments
    input_parameters{1}.exp = "indoor irradiance";
    input_parameters{1}.din = [main_dir '20170510_Tartu/20170510_Digital_Counts/'];
    input_parameters{1}.ser_no = '258';
    input_parameters{1}.fn = glob ([input_parameters{1}.din '**' input_parameters{1}.ser_no '*']);
    input_parameters{1}.sheet = "Irradiance";

    input_parameters{2}.exp = "indoor radiance low light 222";
    input_parameters{2}.din = [main_dir '20170509_Tartu/20170509_Digital_Counts/'];
    input_parameters{2}.ser_no = '222';
    input_parameters{2}.fn = glob ([input_parameters{2}.din '*_LL*' input_parameters{2}.ser_no '*']);
    input_parameters{2}.sheet = "low radiance_222";
    
    input_parameters{3}.exp = "indoor radiance high light 222";
    input_parameters{3}.din = [main_dir '20170509_Tartu/20170509_Digital_Counts/'];
    input_parameters{3}.ser_no = '222';
    input_parameters{3}.fn = glob ([input_parameters{3}.din '*_LH*' input_parameters{3}.ser_no '*']);
    input_parameters{3}.sheet = "high radiance_222";
    
    input_parameters{4}.exp = "indoor radiance low light 223";
    input_parameters{4}.din = [main_dir '20170509_Tartu/20170509_Digital_Counts/'];
    input_parameters{4} .ser_no= '223';
    input_parameters{4}.fn = glob ([input_parameters{4}.din '*_LL*' input_parameters{4}.ser_no '*']);
    input_parameters{4}.sheet = "low radiance_223";
    
    input_parameters{5}.exp = "indoor radiance high light 223";
    input_parameters{5}.din = [main_dir '20170509_Tartu/20170509_Digital_Counts/'];
    input_parameters{5}.ser_no = '223';
    input_parameters{5}.fn = glob ([input_parameters{5}.din '*_LH*' input_parameters{5}.ser_no '*']);
    input_parameters{5}.sheet = "high radiance_223";

    
for iexp = 1:length(input_parameters)
    f = ['cp '];
    for irec = 1:length(input_parameters{iexp}.fn)
        f = [f, input_parameters{iexp}.fn{irec}, ' '];
    endfor
    
    outdir = ["./Send2TO_indoor/" strrep(input_parameters{iexp}.sheet, ' ', '_') '/'];
    
    if ~isdir(outdir)
        mkdir(outdir);
    endif
    
    system([f outdir])
    
endfor

    
# 
# for iexp = 2:3#1:length(input_parameters)
#     proc_TO_experiment(input_parameters{iexp}, main_dir);
# endfor
# 
