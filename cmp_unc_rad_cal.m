function u_rad_cal = cmp_unc_rad_cal(in, sn)



        #
        # from hsas_calibrate: calibrated_L = (L_CountsLightDat - L_CalDarkDat).*a.*ic.*it_1./it_2;
        # where a is the calibration coefficient, ic is an immersion coefficient, it_1 and it_2 are the integration times during calibration and measurement
            
            
            
            # read uncertainty in rad calibration from pml.xlsx
                din = "../TO_HyerpSAS_Cal_11Nov2017/";
                fnin = [din "pml.xlsx"];

                tmp = xlsread(fnin, ["0" sn], "B11:D190");
                unc.(["sn" sn]).wv = tmp(:,1)';
                unc.(["sn" sn]).k1 = tmp(:,end)'/2/100; # divide by 2 because TO applied k=2, then divide by 100 because it was expressed in percentage
                
                u_rad_cal =  in .* (ones(size(in,1),1)*unc.(["sn" sn]).k1 )     ; # the absolute uncertainty is a fraction of the gain
                




endfunction
