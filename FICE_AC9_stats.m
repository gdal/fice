clear all
close all



load FICE_ac9.oct


f = fieldnames(out);

ap440 = [];
cp440 = [];
ay440 = [];
a488 = [];
c488 = [];
ay412 = [];

for id = 2:length(f)
	
	ap440 = [ap440; out.(f{id}).medians.aTOT(2) - out.(f{id}).medians.aCDOM(2) ] ;
	cp440 = [cp440; out.(f{id}).medians.cTOT(2) - out.(f{id}).medians.cCDOM(2)];
	ay440 = [ay440; out.(f{id}).medians.aCDOM(2) ];
	
	ay412 = [ay412; out.(f{id}).medians.aCDOM(1) ];
	a488 = [a488; out.(f{id}).medians.aTOT(3)  ] ;
	c488 = [c488; out.(f{id}).medians.cTOT(3) ];
	
endfor


[median([ap440'; cp440'; ay440'; ay412'; a488'; c488'], 2) prcrng([ap440'; cp440'; ay440'; ay412'; a488'; c488']')']





