function FICE_write_SeaPRISM_out(out, out2, anc, st2out, srfSPRISM, NIR_CORRECTION)
	

	if NIR_CORRECTION
        fnout = [ "../submitted_output/FRM4SOC_FICE-AAOT_PML_SeaPRISM_" datestr(out2.olci.time(1), "yyyymmdd") ".txt" ]
	else
        fnout = [ "../submitted_output/FRM4SOC_FICE-AAOT_PML_SeaPRISM_" datestr(out2.olci.time(1), "yyyymmdd") "_noNIR.txt" ]
	endif
        fnm2 = fieldnames(out2.SPRISM);
        iRrs = find(cellfun(@isempty, strfind(fnm2, "Rrs"))==0); # index for Rrs data in out2.SPRIMS
        
        # which stations should we report?
            [it2anc it2out] = (ismember(anc.time(st2out), out2.olci.time));
            if all(it2anc==0)
                return
            endif
            it2out = it2out(find(it2out!=0));        
            
		medout = [];
		filenm = ["wavelength_nm "];
		for ir = 1:length(it2out)
			medout = [medout; out2.SPRISM.(fnm2{iRrs(it2out(ir))})'];
			
			# prepare filename string for output
                tmp = strsplit(fnm2{iRrs(it2out(ir))}, {"_"});
                oldtime = [tmp{2} "_" tmp{3}];
                newtime = datestr(datenum([str2num(tmp{2}) 1 str2num(tmp{3})]), "yyyy_mm_dd");
                dtsr = [strrep(fnm2{iRrs(it2out(ir))}, oldtime, newtime) " "];
                filenm = [filenm, dtsr];
		endfor
		
        medout = [srfSPRISM.nom medout'];
        

        fid = fopen(fnout, 'w');
            fputs(fid, filenm);
            fputs(fid, "\n");
            fprintf(fid, fmt = [repmat("%f ", length(srfSPRISM.nom), size(medout,2)-1) repmat("%f\n", length(srfSPRISM.nom),1)],  medout');
        fclose(fid);

	
	
		
	
	
endfunction
