function u_align = cmp_unc_pol_align(in, sn)



    L222.dk.all = [];
    L222.d17.all = [];
    L222.d22.all = [];

    L223.dk.all = [];
    L223.d17.all = [];
    L223.d22.all = [];

     
    
    

    ff = fieldnames(in);
    u_align = nan(size(in.(ff{1}).wv));

    
    for iff=1:length(ff)   

        # selects the datsets used to test for alignemnt and polarisation effects on irradiance 
        if ~isempty(strfind(ff{iff}, "1024")) && strcmp(sn,"258")
            if ~isempty(strfind(ff{iff}, "D")) 
                ES258.dk.avg = mean(in.(ff{iff}).EScal); 
                ES258.dk.ste = std(in.(ff{iff}).EScal)/size(in.(ff{iff}).EScal,1); 
            elseif isempty(strfind(ff{iff}, "R"))
                ES258.rot1.avg = mean(in.(ff{iff}).EScal); 
                ES258.rot1.ste = std(in.(ff{iff}).EScal)/size(in.(ff{iff}).EScal,1); 
            else
                ES258.rot2.avg = mean(in.(ff{iff}).EScal); 
                ES258.rot2.ste = std(in.(ff{iff}).EScal)/size(in.(ff{iff}).EScal,1); 
            endif
        endif
        
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 222 low light
        # I selected only data with an int time of 2048 ms, because the other int time was only measured at one distance
        if ~isempty(strfind(ff{iff}, "2048")) && strcmp(sn,"222") && ~isempty(strfind(ff{iff}, "_LL"))
            if ~isempty(strfind(ff{iff}, "D")) 
                L222.dk.all = [L222.dk.all; mean(in.(ff{iff}).LIcal)]; # this is needed becuse there are multiple dark files
                L222.dk.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_17_"))
                L222.d17.all = [L222.d17.all; mean(in.(ff{iff}).LIcal)]; # this is needed in case there are multiple files
                L222.d17.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_22_"))
                L222.d22.all = [L222.d22.all; mean(in.(ff{iff}).LIcal)]; # this is needed in case there are multiple files
                L222.d22.fn{iff} = ff{iff};
                
            endif
        endif
        
        
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 222 high light
        # I selected only data with an int time of 512 ms, to avoid problems with the non-linearity of the instrument
        if ~isempty(strfind(ff{iff}, "512")) && strcmp(sn,"222") && ~isempty(strfind(ff{iff}, "_LH"))
        
            if ~isempty(strfind(ff{iff}, "D")) 
                L222.dk.all = [L222.dk.all; mean(in.(ff{iff}).LIcal)]; # this is needed becuse there are multiple dark files
                L222.dk.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_17_"))
                L222.d17.all = [L222.d17.all; mean(in.(ff{iff}).LIcal)]; # this is needed in case there are multiple files
                L222.d17.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_22_"))
                L222.d22.all = [L222.d22.all; mean(in.(ff{iff}).LIcal)]; # this is needed in case there are multiple files
                L222.d22.fn{iff} = ff{iff};
                
            endif
        endif
        
        
        
        
        
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 223 low light
        # I selected only data with an int time of 2048 ms, to avoid problems with the non-linearity of the instrument
        if ~isempty(strfind(ff{iff}, "2048")) && strcmp(sn,"223") && ~isempty(strfind(ff{iff}, "_LL"))
        
            if ~isempty(strfind(ff{iff}, "D")) 
                L223.dk.all = [L223.dk.all; mean(in.(ff{iff}).LTcal)]; # this is needed becuse there are multiple dark files
                L223.dk.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_17_"))
                L223.d17.all = [L223.d17.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d17.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_22_"))
                L223.d22.all = [L223.d22.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d22.fn{iff} = ff{iff};
                
            endif
        endif
 
 
 
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 223 high light
        # I selected only data with an int time of 2048 ms, to avoid problems with the non-linearity of the instrument
        if ~isempty(strfind(ff{iff}, "512")) && strcmp(sn,"223") && ~isempty(strfind(ff{iff}, "_LH"))
        
            if ~isempty(strfind(ff{iff}, "D")) 
                L223.dk.all = [L223.dk.all; mean(in.(ff{iff}).LTcal)]; # this is needed becuse there are multiple dark files
                L223.dk.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_17_"))
                L223.d17.all = [L223.d17.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d17.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_22_"))
                L223.d22.all = [L223.d22.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d22.fn{iff} = ff{iff};
                
            endif
        endif
        
        
        
    endfor


    
    
    
    
    
    
    
    
    if strcmp(sn,"222") 
    
        L222.dk.avg = mean(L222.dk.all);
        L222.d17.avg = mean(L222.d17.all);
        L222.d22.avg = mean(L222.d22.all);    
            
        # I compute the uncertainty due to alignemnt and polarisation
        # by taking the std of the two rotation measurements
        # No need to correct for dark, because it should be independent of rotation and would cancel out in the difference
            u_align = std( [L222.d17.avg; L222.d22.avg] )./( mean([L222.d17.avg; L222.d22.avg])-L222.dk.avg) *100  ; # [%]
     
            
    elseif strcmp(sn,"223") 
  
        L223.dk.avg = mean(L223.dk.all);
        L223.d17.avg = mean(L223.d17.all);
        L223.d22.avg = mean(L223.d22.all);
            
        # I compute the uncertainty due to alignemnt and polarisation
        # by taking the std of the two rotation measurements
        # No need to correct for dark, because it should be independent of rotation and would cancel out in the difference
            u_align = std( [L223.d17.avg; L223.d22.avg] )./( mean([L223.d17.avg; L223.d22.avg])-L223.dk.avg) *100  ; # [%]
        
        
    elseif strcmp(sn,"258")                 
        # I compute the uncertainty due to alignemnt and polarisation
        # by taking the std of the two rotation measurements
        # No need to correct for dark, because it should be independent of rotation and would cancel out in the difference
            u_align = std( [ES258.rot1.avg; ES258.rot2.avg] )./( mean([ES258.rot1.avg;ES258.rot2.avg])-ES258.dk.avg) *100  ; # [%]

    endif
        
        
        
    

endfunction
