function [out olci_wv olci_int] = cmpOLCIbands(in,wv)
#
# out = cmpOLCIbands(in,wv)
#
# convert the spectrum in(wv) into OLCI bands values


olci_wv = [400 442.5 490 560 665 778.9 865]';
olci_width = [15 10 10 10 10 15 20]';

olci_int = [olci_wv-olci_width/2, olci_wv+olci_width/2];

out = zeros(size(olci_wv));
for iwv = 1:length(olci_wv)

    iw = find( wv>=olci_int(iwv,1) & wv<=olci_int(iwv,2) );

    out(iwv) = mean(in(iw));

endfor







endfunction




