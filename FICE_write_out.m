function FICE_write_out(out, out2, anc, st2out, NIR_CORRECTION)

	
	fnxls_tmp = "../submitted_output/FRM4SOC_FICE-AAOT_PML.xlsx";
	
    if NIR_CORRECTION
        fnxls = [ fnxls_tmp(1:end-5) "_" datestr(out2.olci.time(1), "yyyymmdd") ".xlsx" ];
    else
        fnxls = [ fnxls_tmp(1:end-5) "_" datestr(out2.olci.time(1), "yyyymmdd") "_noNIR.xlsx" ];
    endif	
	
	stri = ["cp " fnxls_tmp " "  fnxls    ];
	system(stri);
	
	fnm2 = fieldnames(out2.olci);
	iLI = find(cellfun(@isempty, strfind(fnm2, "HSL222"))==0);
	iLT = find(cellfun(@isempty, strfind(fnm2, "HSL223"))==0);
	iES = find(cellfun(@isempty, strfind(fnm2, "HSE258"))==0);
# 	iRrs = find(cellfun(@isempty, strfind(fnm2, "Rrs_NIR"))==0);
	iRrs = find(cellfun(@isempty, strfind(fnm2, "Rrs"))==0);
	
	iwv = 1:19; # wv to report
	
	
	# which stations should we report
		[it2anc it2out] = (ismember(anc.time(st2out), out2.olci.time));
		if all(it2anc==0)
			return
		endif
		it2out = it2out(find(it2out!=0))
		

		iout = 12;
		# write Rrs	
		% for irec = 1:length(it2out)
			
		disp("writing Rrs data to xls...");
		fflush(stdout);
					
		ins = "Rrs";
		rad_med = [ins "cal_med"];
		rad_sig = [ins "cal_sig"];
		rng = sprintf("A%u:A%u", iout, iout+2*length(it2out)-1);
		RSTATUS = xlswrite (fnxls, repmat({[ins "_median"];[ins "_stdev"]},length(it2out),1), "preliminary results", rng);
		disp("....written label");
		fflush(stdout);
					
	
		rng = sprintf("B%u:B%u", iout, iout+2*length(it2out)-1);
		% RSTATUS = xlswrite (fnxls, strsplit(sprintf("s%u\n",anc.station(it2out)), "\n")(1:end-1), "preliminary results", rng)
		tmp = nan(2*length(it2anc),1);
		tmp(1:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc); 
		tmp(2:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc);
		RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
		disp("....written station number");
		fflush(stdout);
		
	
		rng = sprintf("D%u:D%u", iout, iout+2*length(it2out)-1);
		tmp = datestr(out2.olci.time(it2out), "yyyy-mm-ddTHH:MM:SS.SSZ");
		dmp = tmp;
		dmp(1:2:2*length(it2out),:) = tmp;
		dmp(2:2:2*length(it2out),:) = tmp;
		RSTATUS = xlswrite (fnxls, mat2cell(dmp, ones(2*length(it2out),1), length(tmp(1,:))), "preliminary results", rng);
		disp("....written time");
		fflush(stdout);
		
		
		rng = sprintf("E%u:E%u", iout, iout+2*length(it2out)-1);
		tmp = nan(2*length(it2out),1);
		tmp(1:2:2*length(it2out)) = out2.olci.ws(it2out); 
		tmp(2:2:2*length(it2out)) = out2.olci.ws(it2out);
		RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
		disp("....written wind speed");
		fflush(stdout);
		
		rng = sprintf("G%u:H%u", iout, iout+2*length(it2out)-1);
		tmp = nan(2*length(it2out),2);
		tmp(1:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:); 
		tmp(2:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:);
		RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
		disp("....written solar angles");
		fflush(stdout);

		
		
		rng = sprintf("I%u:AA%u", iout, iout+2*length(it2out)-1);
		medout = stdevo = [];
		for ir = 1:length(it2out)
			medout = [medout; out2.olci.(fnm2{iRrs(it2out(ir))})(iwv)'];
			stdevo = [stdevo; nan*out2.olci.(fnm2{iRrs(it2out(ir))})(iwv)'];
		endfor
		tmp = [];
		tmp(1:2:2*length(it2out),:) = medout;
		tmp(2:2:2*length(it2out),:) = stdevo;
		RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
		disp("....written Rrs");
		fflush(stdout);
		

	
		



			iout = iout+2*length(it2out);
			
						
			disp("writing LI data to xls...");
			fflush(stdout);
					
			ins = "LI";
			rad_med = [ins "cal_med"];
			rad_sig = [ins "cal_sig"];
		
			rng = sprintf("A%u:A%u", iout, iout+2*length(it2out)-1);
			RSTATUS = xlswrite (fnxls, repmat({[ins "_median"];[ins "_stdev"]},length(it2out),1), "preliminary results", rng);

			rng = sprintf("B%u:B%u", iout, iout+2*length(it2out)-1);
			% RSTATUS = xlswrite (fnxls, strsplit(sprintf("s%u\n",anc.station(it2out)), "\n")(1:end-1), "preliminary results", rng)
			tmp = nan(2*length(it2anc),1);
			tmp(1:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc); 
			tmp(2:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			disp("....written station number");
			fflush(stdout);
			
			rng = sprintf("D%u:D%u", iout, iout+2*length(it2out)-1);
			tmp = datestr(out2.olci.time(it2out), "yyyy-mm-ddTHH:MM:SS.SSZ");
			dmp = tmp;
			dmp(1:2:2*length(it2out),:) = tmp;
			dmp(2:2:2*length(it2out),:) = tmp;
			RSTATUS = xlswrite (fnxls, mat2cell(dmp, ones(2*length(it2out),1), length(tmp(1,:))), "preliminary results", rng);
			
			rng = sprintf("E%u:E%u", iout, iout+2*length(it2out)-1);
			tmp = nan(2*length(it2out),1);
			tmp(1:2:2*length(it2out)) = out2.olci.ws(it2out); 
			tmp(2:2:2*length(it2out)) = out2.olci.ws(it2out);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			
			rng = sprintf("G%u:H%u", iout, iout+2*length(it2out)-1);
			tmp = nan(2*length(it2out),2);
			tmp(1:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:); 
			tmp(2:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			
			rng = sprintf("I%u:AA%u", iout, iout+2*length(it2out)-1);
			medout = stdevo = [];
			for ir=1:length(it2out)
				medout = [medout; out2.olci.(fnm2{iLI(it2out(ir))}).(rad_med)(iwv)];
				stdevo = [stdevo; out2.olci.(fnm2{iLI(it2out(ir))}).(rad_sig)(iwv)];
			endfor
			tmp = [];
			tmp(1:2:2*length(it2out),:) = medout;
			tmp(2:2:2*length(it2out),:) = stdevo;
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			disp("...done");
			fflush(stdout);
					
	




			iout = iout+2*length(it2out);
			
						
			disp("writing LT data to xls...");
			fflush(stdout);
					
			ins = "LT";
			rad_med = [ins "cal_med"];
			rad_sig = [ins "cal_sig"];
		
			rng = sprintf("A%u:A%u", iout, iout+2*length(it2out)-1);
			RSTATUS = xlswrite (fnxls, repmat({[ins "_median"];[ins "_stdev"]},length(it2out),1), "preliminary results", rng);
			
			rng = sprintf("B%u:B%u", iout, iout+2*length(it2out)-1);
			% RSTATUS = xlswrite (fnxls, strsplit(sprintf("s%u\n",anc.station(it2out)), "\n")(1:end-1), "preliminary results", rng)
			tmp = nan(2*length(it2anc),1);
			tmp(1:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc); 
			tmp(2:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
	
			rng = sprintf("D%u:D%u", iout, iout+2*length(it2out)-1);
			tmp = datestr(out2.olci.time(it2out), "yyyy-mm-ddTHH:MM:SS.SSZ");
			dmp = tmp;
			dmp(1:2:2*length(it2out),:) = tmp;
			dmp(2:2:2*length(it2out),:) = tmp;
			RSTATUS = xlswrite (fnxls, mat2cell(dmp, ones(2*length(it2out),1), length(tmp(1,:))), "preliminary results", rng);
			
			rng = sprintf("E%u:E%u", iout, iout+2*length(it2out)-1);
			tmp = nan(2*length(it2out),1);
			tmp(1:2:2*length(it2out)) = out2.olci.ws(it2out); 
			tmp(2:2:2*length(it2out)) = out2.olci.ws(it2out);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			
			rng = sprintf("G%u:H%u", iout, iout+2*length(it2out)-1);
			tmp = nan(2*length(it2out),2);
			tmp(1:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:); 
			tmp(2:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			
			rng = sprintf("I%u:AA%u", iout, iout+2*length(it2out)-1);
			medout = stdevo = [];
			for ir=1:length(it2out)
				medout = [medout; out2.olci.(fnm2{iLT(it2out(ir))}).(rad_med)(iwv)];
				stdevo = [stdevo; out2.olci.(fnm2{iLT(it2out(ir))}).(rad_sig)(iwv)];
			endfor
			tmp = [];
			tmp(1:2:2*length(it2out),:) = medout;
			tmp(2:2:2*length(it2out),:) = stdevo;
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			disp("...done");
			fflush(stdout);
					
			
	




			iout = iout+2*length(it2out);
			
						
			disp("writing ES data to xls...");
			fflush(stdout);
					
			ins = "ES";
			rad_med = [ins "cal_med"];
			rad_sig = [ins "cal_sig"];
		
			rng = sprintf("A%u:A%u", iout, iout+2*length(it2out)-1);
			RSTATUS = xlswrite (fnxls, repmat({[ins "_median"];[ins "_stdev"]},length(it2out),1), "preliminary results", rng);
			
			rng = sprintf("B%u:B%u", iout, iout+2*length(it2out)-1);
			% RSTATUS = xlswrite (fnxls, strsplit(sprintf("s%u\n",anc.station(it2out)), "\n")(1:end-1), "preliminary results", rng)
			tmp = nan(2*length(it2anc),1);
			tmp(1:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc); 
			tmp(2:2:2*length(find(it2anc==1))) = anc.station(st2out)(it2anc);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);

						
			rng = sprintf("D%u:D%u", iout, iout+2*length(it2out)-1);
			tmp = datestr(out2.olci.time(it2out), "yyyy-mm-ddTHH:MM:SS.SSZ");
			dmp = tmp;
			dmp(1:2:2*length(it2out),:) = tmp;
			dmp(2:2:2*length(it2out),:) = tmp;
			RSTATUS = xlswrite (fnxls, mat2cell(dmp, ones(2*length(it2out),1), length(tmp(1,:))), "preliminary results", rng);
			
			rng = sprintf("E%u:E%u", iout, iout+2*length(it2out)-1);
			tmp = nan(2*length(it2out),1);
			tmp(1:2:2*length(it2out)) = out2.olci.ws(it2out); 
			tmp(2:2:2*length(it2out)) = out2.olci.ws(it2out);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			
			rng = sprintf("G%u:H%u", iout, iout+2*length(it2out)-1);
			tmp = nan(2*length(it2out),2);
			tmp(1:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:); 
			tmp(2:2:2*length(it2out),:) = [out2.olci.saa', out2.olci.sza'](it2out,:);
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			
			rng = sprintf("I%u:AA%u", iout, iout+2*length(it2out)-1);
			medout = stdevo = [];
			for ir=1:length(it2out)
				medout = [medout; out2.olci.(fnm2{iES(it2out(ir))}).(rad_med)(iwv)];
				stdevo = [stdevo; out2.olci.(fnm2{iES(it2out(ir))}).(rad_sig)(iwv)];
			endfor
			tmp = [];
			tmp(1:2:2*length(it2out),:) = medout;
			tmp(2:2:2*length(it2out),:) = stdevo;
			RSTATUS = xlswrite (fnxls, tmp, "preliminary results", rng);
			disp("...done");
			fflush(stdout);
					
	
			% iout = iout+1;
			
		% endfor
	
	
	
	
		
	
	
endfunction
