# prepare FICE output to be submitted
clear all
close all
struct_levels_to_print(0)

#graphics_toolkit('qt');

pkg unload signal netcdf io control

pkg load netcdf io

warning off
# 
# addpath(/Users/gdal/Cruises/AMT26/source/radiometry/hsas.source
# addpath /Users/gdal/Cruises/AMT26/source/radiometry/hsas.source/Functions
# addpath /Users/gdal/Cruises/AMT26/source/radiometry/hsas.source/Lib

NIR_CORRECTION = true; % set this to false if you want to not apply NIR correction
JULY_15_16 = true; % set this to true if you want to process data from those 2 days that were excluded from the analysis


addpath(genpath(tilde_expand("~/Dropbox/Octave_functions/rad_functions")))

% sn = {"222", "223", "258"}; # instrument serial numbers
wv = [350:2:860]'; # common set of wavelengths



#### SeaPRISM channels and flat spectral response functions
    srfSPRISM.nom = [412, 443, 488, 531, 551, 667]';
    for iwsp = 1:length(srfSPRISM.nom)
        srfSPRISM.wv(:,iwsp) = [srfSPRISM.nom(iwsp)-5:1:srfSPRISM.nom(iwsp)+5]'; # create flat response +/-5nm around nominal wavelengths 
        srfSPRISM.d(:,iwsp) = ones(size(srfSPRISM.wv(:,iwsp))); # create flat response +/-5nm around nominal wavelengths 
    endfor
	
	for inom =1:length(srfSPRISM.nom) # interpolate to common set of wavelengths
		srfSPRISM.id(:,inom) = interp1(srfSPRISM.wv(:,inom), srfSPRISM.d(:,inom), wv);
	endfor

	
	
# coordinates of AAOT
	gps.lat = 45+(18+50/60)/60; # 45degN 18' 50"
	gps.lon = 12+(30+29/60)/60; # 12degE 30' 29"





din = "Processed/";
ddays = glob([din "2*"]);


# read Log File with environmental parameters recorded by AAOT
	anc = FICE_rd_xls_log();



# read OLCI Spectral Response Functions
	fnSRF = "../Data/S3A_OL_SRF_20160713_mean_rsr.nc4";
	srf.d = ncread(fnSRF, "mean_spectral_response_function");
	srf.wv = ncread(fnSRF, "mean_spectral_response_function_wavelength");
	srf.nom = ncread(fnSRF, "nominal_centre_wavelength");
	
	for inom =1:length(srf.nom) # interpolate to common set of wavelengths
		srf.id(:,inom) = interp1(srf.wv(:,inom), srf.d(:,inom), wv);
	endfor

	
# read list of good stations
    if JULY_15_16
        st2out = [1:66]; # This is to process also July 15th and 16th (iday = 3:4 below)
    else
        st2out = [1:29, 61:66]; # based on TO submitted file
    endif
        







for iday = 1:5
% iday = 2;

	fnoct = glob([ddays{iday} "/*.oct"]){1};
	load(fnoct)
	

	
	

	wv = [350:2:860]'; # common set of wavelengths (reset it because load may modify it)



	nm = fieldnames(out);
	# note nm(1:2:end) are all the file with dark counts
	
	nm_data = nm(2:2:end);
	nm_dark = nm(1:2:end);
	
	n = length(nm_data);
	
	
	disp("processing data...");
	fflush(stdout);
	for idata = 1:n
		disp(nm_data{idata});
		fflush(stdout);
		# add ancillary data to the radiometric data structure
			# find correct date and time in anc file
				imatch = find(abs(out.(nm_data{idata}).time(1)-anc.time)==min(abs(out.(nm_data{idata}).time(1)-anc.time)));
		
			flds = fieldnames(anc);
			for ifld = 1:length(flds)
				out.(nm_data{idata}).(flds{ifld}) = anc.(flds{ifld})(imatch);
			endfor
			
			
		
		# compute sun geometry
	        [solar_azimuth solar_elevation] = SolarAzEl(out.(nm_data{idata}).time, gps.lat, gps.lon, Altitude=0);

	        out.(nm_data{idata}).saa = solar_azimuth;
	        out.(nm_data{idata}).sza = 90 - solar_elevation;
			out.(nm_data{idata}).gps.lat = gps.lat;
			out.(nm_data{idata}).gps.lon = gps.lon;
		
		# save sensor geometry
			out.(nm_data{idata}).phi = anc.phi(imatch); # set manually at each station and recorded in log file
			out.(nm_data{idata}).vza = 40.; # set by the angle of the stainless steel bracket
		
		
		# figure out which instrument we are using and set field name for output
		if isstrprop(nm_data{idata}(end), 'digit')
			instr_sn = nm_data{idata}(end-2:end);
		else	
			instr_sn = nm_data{idata}(end-3:end-1);
		endif
		
			switch instr_sn
				case{"222"}
				 instr_nm = "LI";
 		 		# create a vector with the time of each station	
 		 			out2.olci.time(idata) = out.(nm_data{idata}).time;
	 			case{"223"}
	 			 instr_nm = "LT";
	 			case{"258"}
	 			 instr_nm = "ES";
			endswitch 		
	
		# correct data for dark
			out2.(nm_data{idata}).(["raw_" instr_nm "cal_med"]) = out.(nm_data{idata}).([instr_nm "cal_med"]) - out.(nm_dark{idata}).([instr_nm "cal_med"]);
			out2.(nm_data{idata}).(["raw_" instr_nm "cal_sig"]) = sqrt(out.(nm_data{idata}).([instr_nm "cal_sig"]).^2 + out.(nm_dark{idata}).([instr_nm "cal_sig"]).^2);
			out2.(nm_data{idata}).raw_wv = out.(nm_data{idata}).wv;
		
		
		# interpolate to common wv set
			out2.(nm_data{idata}).([instr_nm "cal_med"]) = interp1(out2.(nm_data{idata}).raw_wv, out2.(nm_data{idata}).(["raw_" instr_nm "cal_med"]), wv);
			out2.(nm_data{idata}).([instr_nm "cal_sig"]) = interp1(out2.(nm_data{idata}).raw_wv, out2.(nm_data{idata}).(["raw_" instr_nm "cal_sig"]), wv);		
		
		
		
		
		# apply OLCI spectral response functions
			for isrf = 1:length(srf.nom)
				innan = find(!isnan(srf.id(:,isrf)));
				out2.olci.(nm_data{idata}).([instr_nm "cal_med"])(isrf) = trapz(wv(innan), srf.id(innan,isrf).*out2.(nm_data{idata}).([instr_nm "cal_med"])(innan) )./trapz(wv(innan), srf.id(innan,isrf));
				out2.olci.(nm_data{idata}).([instr_nm "cal_sig"])(isrf) = trapz(wv(innan), srf.id(innan,isrf).*out2.(nm_data{idata}).([instr_nm "cal_sig"])(innan) )./trapz(wv(innan), srf.id(innan,isrf));
			endfor # isrf
		
		
		# apply SeaPRISM spectral response functions
			for isrf = 1:length(srfSPRISM.nom)
				innan = find(!isnan(srfSPRISM.id(:,isrf)));
				out2.SPRISM.(nm_data{idata}).([instr_nm "cal_med"])(isrf) = trapz(wv(innan), srfSPRISM.id(innan,isrf).*out2.(nm_data{idata}).([instr_nm "cal_med"])(innan) )./trapz(wv(innan), srfSPRISM.id(innan,isrf));
				out2.SPRISM.(nm_data{idata}).([instr_nm "cal_sig"])(isrf) = trapz(wv(innan), srfSPRISM.id(innan,isrf).*out2.(nm_data{idata}).([instr_nm "cal_sig"])(innan) )./trapz(wv(innan), srfSPRISM.id(innan,isrf));
			endfor # isrf

			
			
			# save solar geometry
			out2.olci.saa(idata) = median(out.(nm_data{idata}).saa);
			out2.olci.sza(idata) = median(out.(nm_data{idata}).sza);
		
			out2.olci.ws(idata) = median(out.(nm_data{idata}).wind_speed_m2s);
		
		
			% # this is the version for continuous data
% 			if VBS, disp("Filter based on Lw(NIR) data"); fflush(stdout); endif
  % L1_f = hsas_filter_sensors_using_Lt_data_v2(L1_f, L1_f, 'vaa_ths');
    
	
  # 
		
		
	endfor #idata
	
	out2.wv = wv;	
	out2.olci.wv = srf.nom;	

% 			if VBS, disp("Filter based on Lw(NIR) data"); fflush(stdout); endif
  % L1_f = hsas_filter_sensors_using_Lt_data_v2(L1_f, L1_f, 'vaa_ths');
    
	
		


	# find LI field to which we associate a rho value
		fnm = fieldnames(out);
		fnm2 = fieldnames(out2);
		iLI1 = find(cellfun(@isempty, strfind(fnm, "HSL222"))==0);
		iLI = find(cellfun(@isempty, strfind(fnm2, "HSL222"))==0);
		iLT = find(cellfun(@isempty, strfind(fnm2, "HSL223"))==0);
		iES = find(cellfun(@isempty, strfind(fnm2, "HSE258"))==0);

	disp("computing Rrs...");
	fflush(stdout);




	clf
	hold on
	Rrs = [];
	nir = find(wv >=750 & wv<=800);

	for it = 1:length(iLI)

		xx = strsplit(fnm{it}, "_");

		# find corresponding anc record
			imct = find(abs(out.(fnm{iLI1(1)}).time-anc.time)==min(abs(out.(fnm{iLI1(it)}).time-anc.time)));

		# new structure field for Rrs
			fld_nm = [xx{1} "_" xx{2} "_" xx{3} "_" datestr(out.(fnm{iLI1(it)}).time, "HHMMSS") "_Rrs_M99"];
		
            disp(fld_nm);
			fflush(stdout);
		
			# RHO: from Mobley 1999
				hsas_FOV = 23; # [degrees] full angle
# # BROKEN LUT				out.(fnm{iLI1(it)}) = hsas_extract_rho_FOV(out.(fnm{iLI1(it)}), hsas_FOV, "wind_speed_m2s"); # this is Mobley 1999
				out.(fnm{iLI1(it)}) = hsas_extract_rho_AeronetOC(out.(fnm{iLI1(it)}), "wind_speed_m2s");
				out2.(fnm2{iLI(it)}).rho_M99 = out.(fnm{iLI1(it)}).rho;
				% out.(fnm{iLI(it)}) = rmfield(out.(fnm{iLI(it)}), "rho");

# 			# RHO: from NIR
# 				out.(fnm{iLI1(it)}) = hsas_extract_rho_FOV(out.(fnm{iLI1(it)}), hsas_FOV, "wind_speed_m2s"); # this is Mobley 1999
# 				out2.(fnm2{iLI(it)}).rho_M99 = out.(fnm{iLI1(it)}).rho;
# 				% out.(fnm{iLI(it)}) = rmfield(out.(fnm{iLI(it)}), "rho");

		# compute Rrs
		if NIR_CORRECTION
			out2.(fld_nm) = ( out2.(fnm2{iLT(it)}).LTcal_med - out2.(fnm2{iLI(it)}).rho_M99*out2.(fnm2{iLI(it)}).LIcal_med )./out2.(fnm2{iES(it)}).EScal_med; #[1/sr]
			out2.(fld_nm) = out2.(fld_nm) - mean(out2.(fld_nm)(nir,:)); # remove NIR reflectance
        else
			out2.(fld_nm) = ( out2.(fnm2{iLT(it)}).LTcal_med - out2.(fnm2{iLI(it)}).rho_M99*out2.(fnm2{iLI(it)}).LIcal_med )./out2.(fnm2{iES(it)}).EScal_med; #[1/sr]
        endif
	
		# apply OLCI spectral response functions
			for isrf = 1:length(srf.nom)
				innan = find(!isnan(srf.id(:,isrf)));
				out2.olci.(fld_nm)(isrf,1) = trapz(wv(innan), srf.id(innan,isrf).*out2.(fld_nm)(innan) )./trapz(wv(innan), srf.id(innan,isrf));
				#out2.olci.(fld_nmNIR)(isrf,1) = trapz(wv(innan), srf.id(innan,isrf).*out2.(fld_nmNIR)(innan) )./trapz(wv(innan), srf.id(innan,isrf));
			endfor # isrf
	
		# apply SeaPRISM spectral response functions
			for isrf = 1:length(srfSPRISM.nom)
				innan = find(!isnan(srfSPRISM.id(:,isrf)));
				out2.SPRISM.(fld_nm)(isrf,1) = trapz(wv(innan), srfSPRISM.id(innan,isrf).*out2.(fld_nm)(innan) )./trapz(wv(innan), srfSPRISM.id(innan,isrf));
			endfor # isrf	
		
			Rrs = [Rrs, out2.(fld_nm)];

	endfor # it	
		

    if NIR_CORRECTION
        Rrs = Rrs - mean(Rrs(nir,:));
    endif

	### plot Rrs ####
		figure(1, 'visible', 'off')
		clf
		subplot(121)
			plot(wv, Rrs)
			xlabel('wavelength [nm]')
			ylabel('R_{rs} [sr^{-1}]')
			grid on
			ylim([-0.001 0.008])
	
		subplot(122)
			plot(wv, Rrs - mean(Rrs(nir,:)))
			xlabel('wavelength [nm]')
			ylabel('R_{rs}-R_{rs}(NIR) [sr^{-1}]')
			grid on

			set(gcf, 'paperposition', [0.25 0.25 12 5])
			ylim([-0.001 0.008])
			
	# save file plot
	if NIR_CORRECTION
		fnout = ['Processed/' dt=datestr(out.(fnm{iLI1(1)}).time(1), 'yyyymmdd') '/Rrs_' dt '.png']
    else
		fnout = ['Processed/' dt=datestr(out.(fnm{iLI1(1)}).time(1), 'yyyymmdd') '/Rrs_' dt '_noNIR.png']
	endif	
    
    print("-dpng", fnout)	

    

	# save data file
	if NIR_CORRECTION
		save("-binary",  [fnoct(1:end-4) "_2.oct"], "out2", "wv")
	else
		save("-binary",  [fnoct(1:end-4) "_2_noNIR.oct"], "out2", "wv")
    endif


	# write data for Gavin
		FICE_write_out(out, out2, anc, st2out, NIR_CORRECTION);


    # write data for SeaPRISM
        FICE_write_SeaPRISM_out(out, out2, anc, st2out, srfSPRISM, NIR_CORRECTION);
        
        

	# clear output variables
		clear out out2



endfor # iday







