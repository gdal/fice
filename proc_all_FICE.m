clear all
close all
graphics_toolkit("gnuplot");

pkg load io

addpath = '~/Dropbox/Octave_functions/hsas/';


main_dir = "./../Data/";
# main_dir = "~/Dropbox/Project/FRM4SOC/TO/Tartu/";


# these are the different input paraemters for all indoor experiments
	input_parameters{1}.exp = "20180713";
	input_parameters{1}.outdir = ["Processed", '/', input_parameters{1}.exp, '/'  ];
    input_parameters{1}.din = [main_dir input_parameters{1}.exp "/"];
    input_parameters{1}.ser_no = 'xxx';
    input_parameters{1}.fn = glob ([input_parameters{1}.din '/Processed/Raw/*' '*dat']);
    input_parameters{1}.sheet = "n/a";
    input_parameters{1}.dir_cal = [main_dir '../Data/cals/'];

	input_parameters{2}.exp = "20180714";
	input_parameters{2}.outdir = ["Processed", '/', input_parameters{2}.exp, '/'  ];
    input_parameters{2}.din = [main_dir input_parameters{2}.exp "/"];
    input_parameters{2}.ser_no = 'xxx';
    input_parameters{2}.fn = glob ([input_parameters{2}.din '/Processed/Raw/*' '*dat']);
    input_parameters{2}.sheet = "n/a";
    input_parameters{2}.dir_cal = [main_dir '../Data/cals/'];

	input_parameters{3}.exp = "20180715";
	input_parameters{3}.outdir = ["Processed", '/', input_parameters{3}.exp, '/'  ];
    input_parameters{3}.din = [main_dir input_parameters{3}.exp "/"];
    input_parameters{3}.ser_no = 'xxx';
    input_parameters{3}.fn = glob ([input_parameters{3}.din '/Processed/Raw/*' '*dat']);
    input_parameters{3}.sheet = "n/a";
    input_parameters{3}.dir_cal = [main_dir '../Data/cals/'];

	input_parameters{4}.exp = "20180716";
	input_parameters{4}.outdir = ["Processed", '/', input_parameters{4}.exp, '/'  ];
    input_parameters{4}.din = [main_dir input_parameters{4}.exp "/"];
    input_parameters{4}.ser_no = 'xxx';
    input_parameters{4}.fn = glob ([input_parameters{4}.din '/Processed/Raw/*' '*dat']);
    input_parameters{4}.sheet = "n/a";
    input_parameters{4}.dir_cal = [main_dir '../Data/cals/'];

	input_parameters{5}.exp = "20180717";
	input_parameters{5}.outdir = ["Processed", '/', input_parameters{5}.exp, '/'  ];
    input_parameters{5}.din = [main_dir input_parameters{5}.exp "/"];
    input_parameters{5}.ser_no = 'xxx';
    input_parameters{5}.fn = glob ([input_parameters{5}.din '/Processed/Raw/*' '*dat']);
    input_parameters{5}.sheet = "n/a";
    input_parameters{5}.dir_cal = [main_dir '../Data/cals/'];
%
%     input_parameters{3}.exp = "indoor radiance high light 222";
%     input_parameters{3}.din = [main_dir '];
%     input_parameters{3}.ser_no = '222';
%     input_parameters{3}.fn = glob ([input_parameters{3}.din '*_LH*' input_parameters{3}.ser_no '*']);
%     input_parameters{3}.sheet = "high radiance_222";
%
%     input_parameters{4}.exp = "indoor radiance low light 223";
%     input_parameters{4}.din = [main_dir '20170509_Tartu/20170509_Digital_Counts/'];
%     input_parameters{4} .ser_no= '223';
%     input_parameters{4}.fn = glob ([input_parameters{4}.din '*_LL*' input_parameters{4}.ser_no '*']);
%     input_parameters{4}.sheet = "low radiance_223";
%
%     input_parameters{5}.exp = "indoor radiance high light 223";
%     input_parameters{5}.din = [main_dir '20170509_Tartu/20170509_Digital_Counts/'];
%     input_parameters{5}.ser_no = '223';
%     input_parameters{5}.fn = glob ([input_parameters{5}.din '*_LH*' input_parameters{5}.ser_no '*']);
%     input_parameters{5}.sheet = "high radiance_223";

    
for iexp = 1:1#1:4#length(input_parameters)
    % f = ['cp '];
%     for irec = 1:length(input_parameters{iexp}.fn)
%         f = [f, input_parameters{iexp}.fn{irec}, ' '];
%     endfor
    
    outdir = ["Processed", '/', input_parameters{iexp}.exp, '/'  ];
    
    if ~isdir(outdir)
        mkdir(outdir);
        mkdir([outdir 'plots/']);		
    endif
    
    % system([f outdir])
    
endfor

    

for iexp = 1:1#length(input_parameters)
    proc_FICE(input_parameters{iexp}, main_dir);
endfor

