# this script computes the number of files used for each instrument and each station
clear all


DALL = "/Users/gdal/Documents/FICE/Data/";

days = glob([DALL "2018*"]);

sn = {"HSL222", "HSL223", "HSE258"};

for id = 1:length(days) # go through each day

	
	for isn = 1:3 # go through each instrument
		fflush(stdout);
		
		fn = glob([days{id} "/Processed/Raw/*" sn{isn} "*"]);
		
		for ifn = 1:length(fn)
			[~, tmp] = system(["cat " fn{ifn} " | wc -l"]);
			disp([strsplit(days{id}, "/"){end} " " sn{isn}  " " strsplit(fn{ifn}, {"/", "-"}){12} " " num2str(str2num(tmp)-5)]);

		endfor
		
	endfor
	
	
	
endfor

