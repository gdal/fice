function u_align = cmp_unc_lin_correction(in, sn)



    L222.i256.all = [];
    L222.i512.all = [];
    L222.i1024.all = [];
    L222.i2048.all = [];

    L223.d17.all = [];
    L223.d22.all = [];

     
    
    

    ff = fieldnames(in);
    u_align = nan(size(in.(ff{1}).wv));

    
    for iff=1:length(ff)   

        # selects the datsets used to determine the non-linearity correction for  
        if ~isempty(strfind(ff{iff}, "1024")) && strcmp(sn,"258")
            if ~isempty(strfind(ff{iff}, "D")) 
                ES258.dk.avg = mean(in.(ff{iff}).EScal); 
                ES258.dk.ste = std(in.(ff{iff}).EScal)/size(in.(ff{iff}).EScal,1); 
            elseif isempty(strfind(ff{iff}, "R"))
                ES258.rot1.avg = mean(in.(ff{iff}).EScal); 
                ES258.rot1.ste = std(in.(ff{iff}).EScal)/size(in.(ff{iff}).EScal,1); 
            else
                ES258.rot2.avg = mean(in.(ff{iff}).EScal); 
                ES258.rot2.ste = std(in.(ff{iff}).EScal)/size(in.(ff{iff}).EScal,1); 
            endif
        endif
        
        # selects the datsets used to determine the non-linearity correction for radiance 222 low light
        # I selected only data with with distance 22    
        # note that here I'm working with digital counts
        if ~isempty(strfind(ff{iff}, "_22_")) && strcmp(sn,"222") && ~isempty(strfind(ff{iff}, "_LL"))
            if isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_1024_"))
                L222.i1024.all = [L222.i1024.all; mean(in.(ff{iff}).LI)]; # this is needed in case there are multiple files
                L222.i1024.fn{iff} = ff{iff};
            elseif ~isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_1024_"))
                L222.i1024.dk = mean(in.(ff{iff}).LI);
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_2048_"))
                L222.i2048.all = [L222.i2048.all; mean(in.(ff{iff}).LI)]; # this is needed in case there are multiple files
                L222.i2048.fn{iff} = ff{iff};
            elseif ~isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_2048_"))
                L222.i2048.dk = mean(in.(ff{iff}).LI);
                
            endif
        endif
        
        
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 222 high light
        # I selected only data with an int time of 512 ms, to avoid problems with the non-linearity of the instrument
        
        if ~isempty(strfind(ff{iff}, "_17_")) && strcmp(sn,"222") && ~isempty(strfind(ff{iff}, "_LH"))
            if isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_512_"))
                L222.i512.all = [L222.i512.all; mean(in.(ff{iff}).LI)]; # this is needed in case there are multiple files
                L222.i512.fn{iff} = ff{iff};
            elseif ~isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_512_"))
                L222.i512.dk = mean(in.(ff{iff}).LI);
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_256_"))
                L222.i256.all = [L222.i256.all; mean(in.(ff{iff}).LI)]; # this is needed in case there are multiple files
                L222.i256.fn{iff} = ff{iff};
            elseif ~isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_256_"))
                L222.i256.dk = mean(in.(ff{iff}).LI);
                
            endif
        endif
        
        
        
        
        
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 223 low light
        # I selected only data with an int time of 2048 ms, to avoid problems with the non-linearity of the instrument
        if ~isempty(strfind(ff{iff}, "2048")) && strcmp(sn,"223") && ~isempty(strfind(ff{iff}, "_LL"))
        
            if ~isempty(strfind(ff{iff}, "D")) 
                L223.dk.all = [L223.dk.all; mean(in.(ff{iff}).LTcal)]; # this is needed becuse there are multiple dark files
                L223.dk.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_17_"))
                L223.d17.all = [L223.d17.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d17.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_22_"))
                L223.d22.all = [L223.d22.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d22.fn{iff} = ff{iff};
                
            endif
        endif
 
 
 
        # selects the datsets used to test for alignemnt and back-reflection effects on radiance 223 high light
        # I selected only data with an int time of 2048 ms, to avoid problems with the non-linearity of the instrument
        if ~isempty(strfind(ff{iff}, "512")) && strcmp(sn,"223") && ~isempty(strfind(ff{iff}, "_LH"))
        
            if ~isempty(strfind(ff{iff}, "D")) 
                L223.dk.all = [L223.dk.all; mean(in.(ff{iff}).LTcal)]; # this is needed becuse there are multiple dark files
                L223.dk.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_17_"))
                L223.d17.all = [L223.d17.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d17.fn{iff} = ff{iff};
                
            elseif isempty(strfind(ff{iff}, "D")) && ~isempty(strfind(ff{iff}, "_22_"))
                L223.d22.all = [L223.d22.all; mean(in.(ff{iff}).LTcal)]; # this is needed in case there are multiple files
                L223.d22.fn{iff} = ff{iff};
                
            endif
        endif
        
        
        
    endfor


    
    
    
    
    
    
    
    
    if strcmp(sn,"222") && ~isempty(strfind(ff{iff}, "_LL"))
    
        L222.i1024.avg = mean(L222.i1024.all,1);
        L222.i2048.avg = mean(L222.i2048.all,1);               
        
        # here I compute the non-linearity coefficient from eq. 2.9 pg 14 in Kowstowski(1997)
         S1 = L222.i1024.avg; 
         S1dk = L222.i1024.dk; 
         t1 = 1024; # [ms]
         C1 = (S1-S1dk)/t1;   
         
         S2 = L222.i2048.avg; 
         S2dk = L222.i2048.dk; 
         t2 = 2048; # [ms]
         C2 = (S2-S2dk)/t2;  
         
         non_linLL = C1./C2;   
            
        # here I compute the non-linearity coefficient from eq. 2.9 pg 14 in Kowstowski(1997)
         aLL = -1./( C1 + C2 ); # this coefficient makes C1./C2.*(1+C1.*aLL).*(1+C2.*aLL) = 1 and could be used to correct for non-linearity
         wv = in.(ff{iff}).wv; 
         save -binary lin_corr_222_LL.oct wv aLL non_linLL
        
        
        keyboard
        
        # Compute the uncertainty due to non-linearity correction
        # by taking the std of the two rotation measurements
        # No need to correct for dark, because it should be independent of rotation and would cancel out in the difference
#             u_align = std( [L222.i1024.avg; L222.i2048.avg] )./( mean([L222.i1024.avg; L222.i2048.avg])-L222.dk.avg) *100  ; # [%]
     
            
            
            
    elseif strcmp(sn,"222") && ~isempty(strfind(ff{iff}, "_LH"))
    
        L222.i256.avg = mean(L222.i256.all,1);
        L222.i512.avg = mean(L222.i512.all,1);    
            
            
            
        
        # here I compute the non-linearity coefficient from eq. 2.9 pg 14 in Kowstowski(1997)
         S1 = L222.i256.avg; 
         S1dk = L222.i256.dk; 
         t1 = 1024; # [ms]
         C1 = (S1-S1dk)/t1;   
         
         S2 = L222.i512.avg; 
         S2dk = L222.i512.dk; 
         t2 = 2048; # [ms]
         C2 = (S2-S2dk)/t2;   
            
         non_linLH = C1./C2;   
            
        # here I compute the non-linearity coefficient from eq. 2.9 pg 14 in Kowstowski(1997)
         aLH = -1./( C1 + C2 );
         wv = in.(ff{iff}).wv; 
         save -binary lin_corr_222_LH.oct wv aLH non_linLH
            
            
            keyboard
            
            
            
            
            
    elseif strcmp(sn,"223") 
  
        L223.dk.avg = mean(L223.dk.all);
        L223.d17.avg = mean(L223.d17.all);
        L223.d22.avg = mean(L223.d22.all);
            
        # I compute the uncertainty due to alignemnt and polarisation
        # by taking the std of the two rotation measurements
        # No need to correct for dark, because it should be independent of rotation and would cancel out in the difference
            u_align = std( [L223.d17.avg; L223.d22.avg] )./( mean([L223.d17.avg; L223.d22.avg])-L223.dk.avg) *100  ; # [%]
        
        
    elseif strcmp(sn,"258")                 
        # I compute the uncertainty due to alignemnt and polarisation
        # by taking the std of the two rotation measurements
        # No need to correct for dark, because it should be independent of rotation and would cancel out in the difference
            u_align = std( [ES258.rot1.avg; ES258.rot2.avg] )./( mean([ES258.rot1.avg;ES258.rot2.avg])-ES258.dk.avg) *100  ; # [%]

    endif
        
        
        
    

endfunction
